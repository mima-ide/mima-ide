# Manual

The Manual is still sparse. But you can already browse the following topics:

* [Mima Assembler](Assembler/index.md)
* [Mima Assembler Quick Reference](Assembler/Quick_Reference.md)
* [User Interface Quick Start](UI/index.md)