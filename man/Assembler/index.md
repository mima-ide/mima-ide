# Mima Assembler

This IDE Supports Standard Mima assembler as well as an
advanced Syntax which makes it easier to write larder programs.

By selecting `Preprocessing Only` from the Build Menu (right click on `Build`),
you can translate advanced syntax to Standard Mima syntax, except for the
assembler directives. Assembler directives are handled in `Link` stage.

A mima assembler file is precessed in the following stages:
+ **Merging**: All imported files are merged together
+ **Preprocessing**: Macros are expanded
+ **Linking**: Segments are arranged, addresses for every Symbol are calculated and symbols are replace with their addresses
+ **Assembling**: Resulting Mima code is assembled into a Memory map
+ **Flashing**: The Memory Map is flashed to the integrated simulator

The **Memory Map** or **Bytecode** might be runnable by other Mima Simulators.

Larger Projects might rely on **Symbol Maps**. These are mima files which only include
information about the location of symbols and the location and size of Segments.

You can select a different Build Target by right clicking on the `Build` Button.

[Got to Assembler Quick Reference](Quick_Reference.md)