# Mima Assembler Quick Reference

All Numbers can be written as decimal or hexadecimal starting with `$`, e.g. `$200` for `0x200`.
Numbers are written in this document as lowercase letters, Symbols and Mnemonics as uppercase letters.

## Instruction Syntax

Every instruction is based on the following syntax: `SYMBOL MNEMONIC PARAM`. 
Symbol is optional and might be followed by a colon. Param is mandatory for
parametrized Instructions. Execution will start at `START` or `MAIN` Symbol.

Variables/Named Addresses can be initialized with `SYMBOL DS n` where `n` is the
initial value for this Memory cell.
```
; This is a comment
START   LDC 0
        NOT
        STV A
; This is a Named Address
A       DS  0
```

## Instructions (Standard Mima)

| Instruction   | Description             |
| ------------- | ----------------------- |
| `LDC n`       | Load constant n         |
| `LDV a`       | Load akku value at a    |
| `STV a`       | Store akku value at a   |
| `ADD a`       | Add value at a to akku  |
| `AND a`       | (value at a) & akku     |
| `OR a`        | (value at a) or akku    |
| `XOR a`       | (value at a) xor akku   |
| `EQL a`       | akku = -1 if (value at a) == akku |
| `JMP a`       | Jump to a               |
| `JMN a`       | Jump to a if akku < 0   |
| `LDIV a`      | Load akku value from address in a |
| `STIV a`      | Store akkue value at address in a |
| `JMS a`       | Jump to subroutine at a |
| `JIND a`      | Jump to address in a    |
| `HALT`        | Stop execution          |
| `NOT`         | One's complement akku   |
| `RAR`         | Rotate Akku right       |

JMS stores return address in `a` and jumps to `a + 1`.

## Macros

*Advanced Syntax*

Macros are simple Templates for common routines. Macros are extended using simple text replacement.
```
%macro: .a .b {
    LDC .a
    STV .b
}

%macro 5 LBL
```
Macro parameters should be prefixed with dots to distinguish from Symbols.

These macro variables are automatically set:
| Variable | Value            |
| -------- | ---------------- |
| `.macro` | The macro's name |
| `.~`     | A random ID unique in every macro substitution. Can be used for internal Labels |

## Conditional Macros

*Advanced Syntax*

Macros names are overloadable, depending on parameters or architecture.
The correct macro is determined depending on conditions:
```
%macro: .a .b [.a = .b] {
    LDC 0
}
%macro: .a .b {
    LDC 1
}
```
The first macro is chosen for `%macro 2 2`, while the second (fallback) macro is used for `%macro 2 3`.
Possible operators are `=`, `!=`, `<`, `>`, `<=`, `>=` and the architecture operator (`arch = mima`). 
Multiple conditions are comma-separated.

## Imports

*Advanced Syntax*

Other files can be imported. Every file is only imported once. The order of file imports is not
specified; especially, imported files are not necessarily pasted at the line where they were imported.
Imports are relative to the current file.
```
%import lib/std.mima
```

## Assembler Directives

| Directive  |  Description                       |   |
| ---------- | ---------------------------------- | - |
| `* = n`    | Start Segment at location n        |   |
| `.auto`    | Automatically position segment     | * |
| `.text`    | Mark as Text segment               | * |
| `.data`    | Mark as Data segment               | * |
| `.size n`  | Minimum size for this segment is n | * |
| `.fill n M`| Fill n memory cells with M. M can be any Mnemonic | * |
| `.symbol N a`| Register Symbol N to be at address a | * |
| `.sub`     | Define a subroutine. Assembles to `DS 0` | * |
| `.ret L`   | Return from subroutine L. Assembles to `JIND L` | * |

[*] *Advanced Syntax*

## Relative Addresses

*Advanced Syntax*

You can reference relative addresses by using the following syntax:
```
JMP 1(IAR)
JMP -1(IP)
```

The offset is calculated from the current IAR/IP position. IR and IAR can be used interchangeable.
This makes loading negative numbers easy:
```
JMP 2(IAR)
DS  -1
LDC -1(IAR)
```
