# User Interface

## Projects

You can create a new Project by clicking on the `+` Button.
Projects can be opened in the `Projects` view accessible in the sidebar.

Every Project consists of multiple files. New Files can be created by clicking on the `+` Button.
Mima code files must have the extension `.mima`.

## Build

You can build your Project by clicking on `Build`. This will build the files
and flash them to the integrated Simulator. Other Build target are accessible by right-clicking on `Build`.

## Debugging

You can run or step through your program when it is flashed. To see the current memory, open
the memory assistant with its tab on the right side. You can set Breakpoints by clicking on
the second-right gutter in the memory assistant or set the Instruction Pointer by right clicking
on the gutter. 