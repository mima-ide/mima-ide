CodeMirror.defineSimpleMode("mimamicro", {

	start: [
        {regex: /^;.*$/, token: "comment"},
		{regex: /^0x[0-9a-f]+\:?/, token: "number", sol: true},
	]

})