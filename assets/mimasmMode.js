// CodeMirror.defineSimpleMode("mimasm", {

// 	start: [
// 		{regex: /;(.*)$/, token: "comment"},
// 		{regex: /\$[0-9a-f]+|[0-9]+/, token: "number"},
// 		{regex: /HALT/, token: "halt"},
// 		{regex: /LDC|LDV|STV|ADD|AND|OR|XOR|EQL|JMP|JMN|JIND|JMS|LDIV|STIV|HALT|NOT|RAR|DS|CALL|RET|LDSP|STSP|LDVR|STVR/, token: "keyword"},
// 		{regex: /\w+/, token: "variable"}
// 	]

// })

export default {
	createArchMode: function (arch, opcodes) {

		let opcodeList = opcodes.concat(["DS"])
								.map(m => [m.toUpperCase(), m.toLowerCase()])
								.flat()
								.sort((a,b) => a.length - b.length)
								.reverse()

		let opcodeRegex = new RegExp(`${opcodeList.join("|")}`)

		CodeMirror.defineSimpleMode("mimasm-" + arch, {

			start: [
				{regex: /;(.*)$/, token: "comment"},
				{regex: /\*\s+=\s+(\$[0-9a-f]+|[0-9]+)/, token: "address"},
				{regex: /\$[0-9a-f]+|[0-9]+/, token: "number"},
				{regex: /HALT|halt/, token: "halt"},
				{regex: /DS|ds/, token: "ds"},
				{regex: /%import (.*)$/, token: "include"},
				{regex: opcodeRegex, token: "keyword"},
				{regex: /\.[a-zA-Z0-9~]+/, token: "macro-variable"},
				{regex: /\w+/, token: "variable"},
				{regex: /%([a-zA-Z0-9]+):/, token: "macro", sol: true, next: 'macrodef'},
				{regex: /%([a-zA-Z0-9]+)/, token: "macro"},
				{regex: /\{|\}/, token: "bracket"},
				
			],
			macrodef: [
				{regex: /\.[a-zA-Z0-9]+/, token: 'macro-variable'},
				{regex: /\[/, token: "bracket", next: 'macrocondition'},
				{regex: /\{/, token: "bracket", next: 'start'},
			],
			macrocondition: [
				{regex: /\.[a-zA-Z0-9]+/, token: 'macro-variable'},
				{regex: /\$[0-9a-f]+|[0-9]+/, token: "number"},
				{regex: /\]/, token: "bracket", next: 'macrodef'},
			],
		
		})


	}
}