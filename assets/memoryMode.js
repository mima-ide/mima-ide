export default {
	createArchMode: function (arch, opcodes) {

		let opcodeList = opcodes.concat(["DS"])
								.map(m => [m.toUpperCase(), m.toLowerCase()])
								.flat()
								.sort((a,b) => a.length - b.length)
								.reverse()

		let opcodeRegex = new RegExp(`${opcodeList.join("|")}`)

		CodeMirror.defineSimpleMode("memory-" + arch, {

			start: [
				{regex: /0x[0-9a-f]+/, token: "address"},
				{regex: /\$[0-9a-f]+|[0-9]+/, token: "number"},
				{regex: /HALT|halt/, token: "halt"},
				{regex: opcodeRegex, token: "keyword"},
				{regex: /\w+/, token: "variable"},
			]
		
		})


	}
}