CodeMirror.defineSimpleMode("mimamicromemory", {

	start: [
        {regex: /0x[0-9a-e]{5}/, next: 'naddr', sol: true}

        // {regex: /^[0-9a-e]{2}\s/, token: "micro-next"},
        // {regex: /^\->\s(.*);/, token: "micro-write"},
        
    ],
    naddr: [
        {regex: /[0-9a-e]{2}\s/, token: "micro-next", next: 'read'},
        {regex: /0x[0-9a-e]{5}/, next: 'naddr', sol: true}
    ],
    read: [
        {regex: /[A-Z\s\,]+/, token: 'micro-read'},
        {regex: /;/, next: 'flags'},
        {regex: /\->/, next: 'write'},
        {regex: /0x[0-9a-e]{5}/, next: 'naddr', sol: true}
    ],
    write: [
        {regex: /[A-Z\s\,]+/, token: 'micro-write'},
        {regex: /;/, next: 'flags'},
        {regex: /0x[0-9a-e]{5}/, next: 'naddr', sol: true}
    ],
    flags: [
        {regex: /[A-Z]+\s\=\s[A-Z0-9]+\s*/, token: 'micro-flag'},
        {regex: /;/, next: 'flags'},
        {regex: /0x[0-9a-e]{5}/, next: 'naddr', sol: true}
    ]

})