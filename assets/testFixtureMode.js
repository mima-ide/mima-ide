CodeMirror.defineSimpleMode("testfixture", {

	start: [

        {regex: /;(.*)$/, token: "comment"},
        {regex: /file [a-zA-Z0-9\.\/]+/, token: "include"},
        {regex: /[a-zA-Z0-9]+:/, token: "macro"},
        {regex: /start|stop|break|set|assert|run|invariant/, token: "keyword"},
        {regex: /\$[0-9a-f]+|[0-9]+/, token: "number"},
        {regex: /\.[a-zA-Z0-9~]+/, token: "macro-variable"},
        {regex: /\w+/, token: "variable"},
        {regex: /\{|\}/, token: "bracket"},
        
    ],

})