export default class Microcode {

    static signalDescription(signals) {

        const s = signals
        let readRegisters = []
        let writeRegisters = []
        let alu = 0

        if (s.Ar) {readRegisters.push("AKKU")}
        
        if (s.Pr) {readRegisters.push("IAR")}
        if (s.Ir) {readRegisters.push("IR")}
        if (s.Dr) {readRegisters.push("SDR")}
        if (s.S) {readRegisters.push("SAR")}
        if (s.X) {readRegisters.push("X")}
        if (s.Y) {readRegisters.push("Y")}

        if (s.Aw) {writeRegisters.push("AKKU")}
        
        if (s.Pw) {writeRegisters.push("IAR")}
        if (s.Iw) {writeRegisters.push("IR")}
        if (s.Dw) {writeRegisters.push("SDR")}
        if (s.Z) {writeRegisters.push("Z")}
        if (s.E) {writeRegisters.push("EINS")}

        alu = ((s.C2 ?1:0) << 2) | ((s.C1 ?1:0) << 1) | (s.C0 ?1:0)
        const aluConstants = {
            0: "NONE",
            1: "ADD",
            2: "ROT",
            3: "AND",
            4: "OR",
            5: "XOR",
            6: "COMP",
            7: "EQL"
        }
        const aluText = alu ? `; ALU = ${aluConstants[alu]}` : ''

        const memoryReadText = s.R ? '; R = 1' : ''
        const memoryWriteText = s.W ? '; W = 1' : ''
        

        const shiftText = writeRegisters.length == 0 && readRegisters.length == 0
            ? ''
            : `${writeRegisters.join(', ')} -> ${readRegisters.join(', ')}`

        return `${shiftText} ${memoryReadText} ${memoryWriteText} ${aluText}`.trim()

    }

    static signalsFromMicrocode(microcode) {

        return {
            Ar : (microcode & 0x8000000) > 0,
            Aw : (microcode & 0x4000000) > 0,
            X  : (microcode & 0x2000000) > 0,
            Y  : (microcode & 0x1000000) > 0,

            Z  : (microcode & 0x0800000) > 0,
            E  : (microcode & 0x0400000) > 0,
            Pr : (microcode & 0x0200000) > 0,
            Pw : (microcode & 0x0100000) > 0,

            Ir : (microcode & 0x0080000) > 0,
            Iw : (microcode & 0x0040000) > 0,
            Dr : (microcode & 0x0020000) > 0,
            Dw : (microcode & 0x0010000) > 0,

            S  : (microcode & 0x0008000) > 0,
            C2 : (microcode & 0x0004000) > 0,
            C1 : (microcode & 0x0002000) > 0,
            C0 : (microcode & 0x0001000) > 0,

            R  : (microcode & 0x0000800) > 0,
            W  : (microcode & 0x0000400) > 0,
        }

    }

    static parseMemoryLine(line) {

        const match = line.match(/^0x([0-9a-f]{1,2}):?\s+0x([0-9a-f]+)(\s*;.*)?$/)
        if (!match) { return null }

        return parseInt(match[2], 16)

    }

}