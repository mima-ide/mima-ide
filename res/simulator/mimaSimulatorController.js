self.importScripts("./memory.js", "./mimasim.js")
self.importScripts("./mimamicsim.js")

let memory = null
let simulator = null
let simulatorName = ""
let startAddress = 0x100;

let controller = {

	setup: async (capabilities) => {

		memory = new Memory()
		if (capabilities.includes('microcode')) {
			simulator = new MimaMicSim(memory, new MimaSim(memory))
		} else {
			simulator = new MimaSim(memory)
		}

		simulator.delegates.break = controller.breakHandler
		simulator.delegates.halt = controller.haltHandler
		simulator.delegates.report = controller.reportHandler

	},

	flash: async contents => {

		memory.flash(contents)

	},

	erase: async _ => {

		memory.erase()
		simulator.breakpoints = []

		controller.reportMemory()

	},

	run: async _ => {

		if (simulator == null) throw("No simulator set up")

		simulator.run()

	},

	step: async _ => {

		if (simulator == null) throw("No simulator set up")

		simulator.step()

	},

	microcodeStep: async _ => {

		if (simulator == null) throw("No simulator set up")
		if (!('microcodeStep' in simulator)) throw("No microcode support")

		simulator.microcodeStep()

	},

	flashMicrocode: async code => {

		if (simulator == null) throw("No simulator set up")
		if (!('microcode' in simulator)) throw("No microcode support")

		simulator.microcode.flash(code)

	},

	reset: async _ => {

		if (simulator == null) throw("No simulator set up")

		simulator.reset(startAddress)

		controller.reportMemory()

	},

	setBreakpoints: async breakpoints => {

		breakpoints = breakpoints ?? []
		simulator.breakpoints = breakpoints

	},

	setRegisters: async registers => {

		for (const register of Object.keys(registers)) {

			simulator.registers[register] = registers[register]

		}

		controller.reportMemory()

	},

	setStartAddress: async address => {

		startAddress = address

	},

	setAddress: async (address, value) => {

		memory.store(address, value)
		controller.reportMemory()

	},

	report: async _ => {

		controller.reportMemory()

	},


	breakHandler: _ => {

		// dump
		controller.reportMemory()

		self.postMessage({action: 'break'})

	},

	haltHandler: _ => {

		// dump
		controller.reportMemory()

		self.postMessage({action: 'halt'})

	},

	reportHandler: _ => {

		controller.reportMemory()

	},

	reportMemory: _ => {
		const dump = memory.dump()
		self.postMessage({action: 'memory', contents: dump})

		let registers = {}
		for (const key of simulator.availableRegisters) {
			registers[key] = simulator.registers[key]
		}

		self.postMessage({action: 'registers', registers})

		if ('remainingSteps' in simulator) {
			self.postMessage({action: 'remaining-steps', steps: simulator.remainingSteps})
		}

		if ('controlSignals' in simulator) {
			self.postMessage({
				action: 'control-signals', 
				controlSignals: simulator.controlSignals
			})
		}

		if ('microcode' in simulator) {
			self.postMessage({action: 'microcode', contents: simulator.microcode.dump()})
		}

	}

}

self.addEventListener('message', e => {

	if (typeof e.data != "object") return;

	let { action = null } = e.data
	if (action == null) return;

	switch (action) {

		case "setup":
			let {capabilities, name} = e.data
			controller.setup(capabilities, name)
			.then(_ => self.postMessage({action: "ready"}))
			.catch(e => self.postMessage({action: "exception", exception: e.toString()}))
			break;

		case "flash":
			let {contents} = e.data
			controller.flash(contents)
			.then(_ => self.postMessage({action: "ready"}))
			.catch(e => self.postMessage({action: "exception", exception: e.toString()}))
			break;

		case "flash-microcode":
			let {contents: micContents} = e.data
			controller.flashMicrocode(micContents)
			.then(_ => self.postMessage({action: "ready"}))
			.catch(e => self.postMessage({action: "exception", exception: e.toString()}))
			break;

		case "run":
			controller.run()
			.catch(e => self.postMessage({action: "exception", exception: e.toString()}))
			break;

		case "step":
			controller.step()
			.catch(e => self.postMessage({action: "exception", exception: e.toString()}))
			break;

		case "microcode-step":
			controller.microcodeStep()
			.catch(e => self.postMessage({action: "exception", exception: e.toString()}))
			break;

		case "reset":
			controller.reset()
			.catch(e => self.postMessage({action: "exception", exception: e.toString()}))
			break;

		case "erase":
			controller.erase()
			.catch(e => self.postMessage({action: "exception", exception: e.toString()}))
			break;

		case "set-breakpoints":
			let {breakpoints} = e.data
			controller.setBreakpoints(breakpoints)
			.catch(e => self.postMessage({action: "exception", exception: e.toString()}))
			break;

		case "set-registers":
			let {registers} = e.data
			controller.setRegisters(registers)
			.catch(e => self.postMessage({action: "exception", exception: e.toString()}))
			break;

		case "set-address":
			let {address: a, value} = e.data
			controller.setAddress(a, value)
			.catch(e => self.postMessage({action: "exception", exception: e.toString()}))
			break;

		case "set-start-address":
			let {address} = e.data
			controller.setStartAddress(address)
			.catch(e => self.postMessage({action: "exception", exception: e.toString()}))
			break;

		case "report":
			controller.report()
			.catch(e => self.postMessage({action: "exception", exception: e.toString()}))
			break;

	}

})
