export default class RemoteSimulator {

	constructor () {

		this.worker = new Worker("./res/simulator/mimaSimulatorController.js")
		this.worker.addEventListener("message", e => this.receiveMessage(e))

		this.delegates = {
			ready: _ => {},
			memory: (data) => {},
			registers: (data) => {},
			break: _ => {},
			halt: _ => {},
			exception: e => {},
			remainingSteps: steps => {},
			controlSignals: signals => {},
			microcode: data => {},
		}

	}

	receiveMessage (e) {

		if (typeof e.data != "object") return;

		let { action = null } = e.data
		if (action == null) return;

		switch (action) {

			case "memory": this.delegates.memory(e.data.contents); break;
			case "registers": this.delegates.registers(e.data.registers); break;
			case "ready": this.delegates.ready(); break;
			case "break": this.delegates.break(); break;
			case "halt": this.delegates.halt(); break;
			case "exception": this.delegates.exception(e.data.exception); break;
			case "remaining-steps": this.delegates.remainingSteps(e.data.steps); break;
			case "control-signals": this.delegates.controlSignals(e.data.controlSignals); break;
			case "microcode": this.delegates.microcode(e.data.contents); break;

		}

	}

	setup (capabilities) {

		this.worker.postMessage({
			action: "setup",
			capabilities: capabilities
		})

	}

	run () {

		this.worker.postMessage({action: 'run'})

	}

	step () {

		this.worker.postMessage({action: 'step'})

	}

	microcodeStep () {

		this.worker.postMessage({action: 'microcode-step'})

	}

	reset () {

		this.worker.postMessage({action: 'reset'})

	}

	erase () {

		this.worker.postMessage({action: 'erase'})

	}

	flash (contents) {

		this.worker.postMessage({
			action: 'flash',
			contents: contents
		})

	}

	flashMicrocode (contents) {

		this.worker.postMessage({
			action: 'flash-microcode',
			contents: contents
		})

	}

	setBreakpoints (breakpoints) {

		this.worker.postMessage({
			action: 'set-breakpoints',
			breakpoints: breakpoints
		})

	}

	setRegisters (registers) {

		this.worker.postMessage({
			action: 'set-registers',
			registers: registers
		})

	}

	setStartAddress (address) {

		this.worker.postMessage({
			action: 'set-start-address',
			address: address
		})

	}

	report() {

		this.worker.postMessage({action: 'report'})

	}

	setAddress (address, value) {

		this.worker.postMessage({action: 'set-address', address, value})

	}

}