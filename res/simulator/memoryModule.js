export default class Memory {

	constructor (capacity) {

		this.capacity = capacity || 0xfffff
		this.contents = {}

	}

	erase () {

		this.contents = {}

	}

	load (address) {

		return (address in this.contents)
			? this.contents[address]
			: 0

	}

	store (address, value) {

		this.contents[address] = value

	}

	dump () {

		let keys = Object.keys(this.contents)

		if (keys.length == 0) return ""

		keys.sort()

		let dump = "";
		for (let key of keys) {
			const address = parseInt(key).toString(16)
			const value = this.contents[key].toString(16)
			dump += `0x${address} 0x${value}\n`
		}

		return dump.substr(0, dump.length -1)

	}

	flash (dump) {

		this.erase()

		const lines = dump.split("\n")
		for (const line of lines) {

			let [address, value] = line.split(' ')

			if (!address || !value) continue;

			this.store(parseInt(address.trim()), parseInt(value.trim()))

		}

	}

}
