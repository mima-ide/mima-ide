const States = {
	HALT: 0,
	RUN: 1,
	BREAK: 2,
	STEP: 3,
}

class MimaSim {

	constructor (memory) {

		this.memory = memory

		this.registers = {
			AKKU: 0,
			IAR: 0x100,
			IR: 0,
		}

		this.state = States.HALT

		this.delegates = {
			unknownCommand: () => {},
			break: () => {},
			halt: () => {},
			report: () => {}
		}

		this.breakpoints = []

		this.remainingSteps = 2000 // to prevent infinite loops

		this.availableRegisters = ["AKKU", "IAR", "IR"]

	}

	reset (startAddress) {

		this.registers.IAR = startAddress == null ? 0x0 : startAddress
		this.remainingSteps = 2000

	}

	run () {

		this.state = States.RUN
		this.remainingSteps = 2000

		while (this.state == States.RUN) {
			this.nextStep()
		}

	}

	step () {

		this.state = States.STEP
		this.remainingSteps = 2000

		this.nextStep()

	}

	nextStep () {

		this.registers.IR = this.memory.load(this.registers.IAR)
		++this.registers.IAR

		--this.remainingSteps

		this.executeInstruction(this.registers.IR)


		if (this.remainingSteps <= 0 || this.state == States.STEP) {
			this.state = States.BREAK
			this.delegates.break()
		}

		if (this.breakpoints.includes(this.registers.IAR)) {
			this.state = States.BREAK
			this.delegates.break()
		}

		if (this.state == States.HALT) {
			this.delegates.halt()
		}

	}

	executeInstruction (instruction) {

		const opcode = (instruction & 0xF00000) >> 20;
		
		const argument = instruction & 0xFFFFF;

		switch (opcode) {
			case 0x0: this.commandLDC(argument); break;
			case 0x1: this.commandLDV(argument); break;
			case 0x2: this.commandSTV(argument); break;
			case 0x3: this.commandADD(argument); break;
			case 0x4: this.commandAND(argument); break;
			case 0x5: this.commandOR(argument); break;
			case 0x6: this.commandXOR(argument); break;
			case 0x7: this.commandEQL(argument); break;
			case 0x8: this.commandJMP(argument); break;
			case 0x9: this.commandJMN(argument); break;
			case 0xA: this.commandLDIV(argument); break;
			case 0xB: this.commandSTIV(argument); break;
			case 0xC: this.commandJMS(argument); break;
			case 0xD: this.commandJIND(argument); break;
			case 0xF: 
				const opcode2 = ((instruction & 0x0F0000) >> 16);
				switch (opcode2) {
					case 0x0: this.commandHALT(); break;
					case 0x1: this.commandNOT(); break;
					case 0x2: this.commandRAR(); break;
					default: this.unknownCommand(); break;
				}
				break;
			default: this.unknownCommand(); break;
		}

	}

	commandLDC (value) {

		this.registers.AKKU = value

	}


	commandLDV (address) {

		// TODO address &= 0xFFFFF
		this.registers.AKKU = this.memory.load(address)

	}

	commandSTV (address) {

		this.memory.store(address, this.registers.AKKU)

	}

	commandADD (address) {

		this.registers.AKKU += this.memory.load(address)
		this.registers.AKKU %= 0x1000000;

	}

	commandAND (address) {

		this.registers.AKKU &= this.memory.load(address)

	}

	commandOR (address) {

		this.registers.AKKU |= this.memory.load(address)

	}

	commandXOR (address) {

		this.registers.AKKU ^= this.memory.load(address)

	}

	commandEQL (address) {

		this.registers.AKKU = (this.memory.load(address) == this.registers.AKKU)
				? -1
				: 0
	}

	commandJMP (address) {

		this.registers.IAR = address

	}

	commandJMN (address) {

		if ((this.registers.AKKU & 0x800000) != 0) {
			console.log("JMN", this.registers.AKKU, this.registers.AKKU & 0x800000)
			this.registers.IAR = address
		}

	}

	commandLDIV (address) {

		this.registers.AKKU = this.memory.load(this.memory.load(address))

	}

	commandSTIV (address) {

		this.memory.store(this.memory.load(address), this.registers.AKKU)

	}

	commandJMS (address) {

		this.memory.store(address, this.registers.IAR)
		this.registers.IAR = address + 1
		
	}

	commandJIND (address) {

		this.registers.IAR = this.memory.load(address)
	
	}

	commandHALT () {
	
		this.state = States.HALT
		
	}

	commandNOT () {

		this.registers.AKKU = ~this.registers.AKKU
		this.registers.AKKU &= 0xffffff

	}

	commandRAR () {

		const carry = this.registers.AKKU & 1
		this.registers.AKKU >>= 1
		this.registers.AKKU |= (carry << 23)
		this.registers.AKKU %= 0x1000000
	
	}

	unknownCommand () {

		this.delegates.unknownCommand()

	}
	

}
