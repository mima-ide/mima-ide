States.MSTEP = 5
DefaultControlSignals = {
	Ar: false,
	Aw: false,
	X: false,
	Y: false,
	Z: false,
	E: false,
	Pr: false,
	Pw: false,
	Ir: false,
	Iw: false,
	Dr: false,
	Dw: false,
	S: false,
	C2: false,
	C1: false,
	C0: false,
	R: false,
	W: false,
}

class MimaMicSim {

	constructor (memory, mima) {

		mima = mima || new MimaSim(memory)

		this.memory = memory
		this.microcode = new Memory(0xff)

		this.microcode.flash(
			"0x0: 0x2108801\n"
			+ "0x1: 0x1400802\n"
			+ "0x2: 0x0001803\n"
			+ "0x3: 0x0a00004\n"
			+ "0x4: 0x0090005\n"

			+ "0x5: 0x0\n"
			+ "0x6: 0x0\n"
			+ "0x7: 0x0\n"
			+ "0x8: 0x27\n"
			+ "0x9: 0x0\n"
			+ "0xa: 0x0\n"
			+ "0xb: 0x0\n"
			+ "0xc: 0x0\n"
			+ "0xd: 0x0\n"
			+ "0xe: 0x0\n"
			+ "0xf: 0x0\n"
			+ "0x10: 0x0\n"
			+ "0x11: 0x0\n"
			+ "0x12: 0x0\n"
			+ "0x13: 0x0\n"
			+ "0x14: 0x0\n"
			+ "0x15: 0x0\n"
			+ "0x16: 0x0\n"

			+ "0x25: 0x0\n"

			+ "0x27: 0x0048828\n" // ; IR -> SAR, R = 1
			+ "0x28: 0x6000829\n" // ; AKKU -> X, R = 1
			+ "0x29: 0x000082a\n" // ; R = 1
			+ "0x2a: 0x101002b\n" // ; SDR -> Y
			+ "0x2b: 0x000102c\n" // ; ALU: ADD
			+ "0x2c: 0x8800000\n" // ; Z -> AKKU
		)

		this.mima = mima

		this.registers = new Registers(this.mima)

		this.controlSignals = {}
		Object.assign(this.controlSignals, DefaultControlSignals)

		this.availableRegisters = this.mima.availableRegisters.concat(
			["SDR", "SAR", "X", "Y", "Z", "EINS", "ALU", "SDR1", "SDR2", "MIAR", "BUS"]
		)

		this.remainingMicrocodeSteps = 0xff

	}	

	set state (value) {

		this.mima.state = value

	}

	get state () {

		return this.mima.state

	}

	set breakpoints (value) {

		this.mima.breakpoints = value

	}

	get breakpoints () {

		return this.mima.breakpoints

	}

	set delegates (value) {

		this.mima.delegates = value

	}

	get delegates () {

		return this.mima.delegates

	}

	set remainingSteps (value) {

		this.mima.remainingSteps = value

	}

	get remainingSteps () {

		return this.mima.remainingSteps

	}

	reset (startAddress) {

		this.registers.IAR = startAddress || 0x100
		this.registers.MIAR = 0x0
		this.remainingSteps = 0xf000
		this.remainingMicrocodeSteps = 0xff

		
		Object.assign(this.controlSignals, DefaultControlSignals)

	}

	run () {

		this.state = States.RUN
		this.remainingSteps = 0xf000

		while (this.state == States.RUN) {
			this.nextStep()
		}

	}

	step () {

		this.state = States.STEP
		this.remainingSteps = 0xf000

		this.nextStep()

	}

	microcodeStep () {

		this.state = States.MSTEP
		this.remainingMicrocodeSteps = 0xff

		while (this.state == States.STEP || this.state == States.RUN || this.state == States.MSTEP) {

			this.nextMicrocodeStep()

		}

		this.delegates.report()

	}

	nextStep () {

		this.remainingMicrocodeSteps = 0xff

		while (this.state == States.STEP || this.state == States.RUN) {
			this.nextMicrocodeStep()
		}

	}

	nextMicrocodeStep () {

		// load microcode 
		const microcode = this.microcode.load(this.registers.MIAR)

		// next address
		const nextAddress = microcode & 0xff
		this.registers.MIAR = nextAddress

		// read control signals
		this.controlSignals.Ar = (microcode & 0x8000000) > 0
		this.controlSignals.Aw = (microcode & 0x4000000) > 0
		this.controlSignals.X  = (microcode & 0x2000000) > 0
		this.controlSignals.Y  = (microcode & 0x1000000) > 0

		this.controlSignals.Z  = (microcode & 0x0800000) > 0
		this.controlSignals.E  = (microcode & 0x0400000) > 0
		this.controlSignals.Pr = (microcode & 0x0200000) > 0
		this.controlSignals.Pw = (microcode & 0x0100000) > 0

		this.controlSignals.Ir = (microcode & 0x0080000) > 0
		this.controlSignals.Iw = (microcode & 0x0040000) > 0
		this.controlSignals.Dr = (microcode & 0x0020000) > 0
		this.controlSignals.Dw = (microcode & 0x0010000) > 0

		this.controlSignals.S  = (microcode & 0x0008000) > 0
		this.controlSignals.C2 = (microcode & 0x0004000) > 0
		this.controlSignals.C1 = (microcode & 0x0002000) > 0
		this.controlSignals.C0 = (microcode & 0x0001000) > 0

		this.controlSignals.R  = (microcode & 0x0000800) > 0
		this.controlSignals.W  = (microcode & 0x0000400) > 0

		// Write Bus
		if (this.controlSignals.Aw) { this.registers.BUS = this.registers.AKKU}
		if (this.controlSignals.Z) { this.registers.BUS = this.registers.Z}
		if (this.controlSignals.E) { this.registers.BUS = this.registers.EINS}
		if (this.controlSignals.Pw) { this.registers.BUS = this.registers.IAR & 0xfffff}
		if (this.controlSignals.Iw) { this.registers.BUS = this.registers.IR}
		if (this.controlSignals.Dw) { this.registers.BUS = this.registers.SDR}

		// Memory
		if (this.controlSignals.W) {
			this.memory.store(this.registers.SAR, this.registers.SDR)
			this.registers.SDR = this.registers.SDR1
			this.registers.SDR1 = this.registers.SDR2
			this.registers.SDR2 = this.registers.BUS
		}

		// ALU
		this.registers.ALU = (this.controlSignals.C2 ? 1 : 0) * 4 
			+ (this.controlSignals.C1 ? 1 : 0) * 2 
			+ (this.controlSignals.C0 ? 1 : 0)

		switch(this.registers.ALU) {
			case 0: 
				this.registers.Z = this.registers.Z
				break;
			case 1:
				this.registers.Z = this.registers.X + this.registers.Y
				break;
			case 2:
				this.registers.Z = this.registers.X
				const carry = this.registers.Z & 1
				this.registers.Z >>= 1
				this.registers.Z |= (carry << 23)
				this.registers.Z %= 0x1000000
				break;
			case 3:
				this.registers.Z = this.registers.X & this.registers.Y
				break;
			case 4:
				this.registers.Z = this.registers.X | this.registers.Y
				break;
			case 5:
				this.registers.Z = this.registers.X ^ this.registers.Y
				break;
			case 6:
				this.registers.Z = ~this.registers.X
				this.registers.X &= 0xffffff
				break;
			case 7:
				this.registers.Z = this.registers.X == this.registers.Y ? -1 : 0
				break;
		}

		// Read Bus
		if (this.controlSignals.Ar) { this.registers.AKKU = this.registers.BUS }
		if (this.controlSignals.X) { this.registers.X = this.registers.BUS }
		if (this.controlSignals.Y) { this.registers.Y = this.registers.BUS }
		if (this.controlSignals.Pr) { this.registers.IAR = this.registers.BUS & 0xfffff }
		if (this.controlSignals.Ir) { this.registers.IR = this.registers.BUS }
		if (this.controlSignals.Dr) { this.registers.SDR = this.registers.BUS }
		if (this.controlSignals.S) { this.registers.SAR = this.registers.BUS  & 0xfffff }

		// Memory
		if (this.controlSignals.R) {
			this.registers.BUS = this.registers.SDR
			this.registers.SDR = this.registers.SDR1
			this.registers.SDR1 = this.registers.SDR2
			this.registers.SDR2 = this.memory.load(this.registers.SAR)
		}



		// if IR was written: branch to corresponding command
		if (this.controlSignals.Ir) {

			let offset = (this.registers.IR & 0xf00000) >> 20
			if (offset == 0xf) {
				offset += (this.registers.IR & 0x0f0000) >> 16
			}

			const address = this.microcode.load(this.registers.MIAR + offset)

			if (address == 0) { // abstract implementation
				this.mima.executeInstruction(this.registers.IR)
				this.registers.MIAR = 0 // back to fetch phase
			} else {
				this.registers.MIAR = address
			}
			
		}


		if (this.registers.MIAR == 0) {
			this.handleInstructionCompleted()
		}
		
		// remaining steps 
		if (this.remainingMicrocodeSteps <= 0 || this.state == States.MSTEP) {
			this.state = States.BREAK
			this.delegates.break()
		}

	}

	handleInstructionCompleted () {

		--this.remainingSteps

		// remaining steps 
		if (this.remainingSteps <= 0 || this.state == States.STEP) {
			this.state = States.BREAK
			this.delegates.break()
		}

		if (this.breakpoints.includes(this.registers.IAR)) {
			this.state = States.BREAK
			this.delegates.break()
		}

		if (this.state == States.HALT) {
			this.delegates.halt()
		}

	}



}

class Registers {

	constructor (mima) {

		this.mima = mima

		this.SDR = 0
		this.SAR = 0
		this.X = 0
		this.Y = 0
		this.Z = 0
		this.EINS = 1
		this.ALU = 0

		this.SDR1 = 0
		this.SDR2 = 0

		this.BUS = 0

		this.MIAR

	}

	set AKKU (value) {

		this.mima.registers.AKKU = value

	}

	get AKKU () {

		return this.mima.registers.AKKU

	}

	set IR (value) {

		this.mima.registers.IR = value

	}

	get IR () {

		return this.mima.registers.IR

	}

	set IAR (value) {

		this.mima.registers.IAR = value

	}

	get IAR () {

		return this.mima.registers.IAR

	}

}