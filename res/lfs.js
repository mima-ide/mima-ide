class LFS {

    constructor() {
        this._callbacks = []
        this._isReady = false
        this._watcher = []
        this.db = null
        var request = indexedDB.open("lfs", 1)
        request.addEventListener("error", evt => {
            console.error('Error opening lfs indexedDB')
        })
        request.addEventListener("success", evt => {
            this.db = evt.target.result
            this._isReady = true
            this._sendReady()
        })
        request.addEventListener("upgradeneeded", evt => {
            let db = evt.target.result

            let dirStore = db.createObjectStore("dirs", {keyPath: "path"})
            db.createObjectStore("files", {keyPath: "path"})

            dirStore.transaction.oncomplete = _ => {
                let os = db.transaction("dirs", "readwrite").objectStore("dirs")
                os.add({path: "/", files: []})
            }
            
        })
    }

    _sendReady() {
        this._callbacks.forEach(cb => cb())
    }

    _sendUpdate(type, path) {
        this._watcher.forEach(cb => cb(type, path))
    }

    ready(cb) {
        if (this._isReady) {
            cb()
        } else {
            this._callbacks.push(cb)
        }
    }

    watch(cb) {
        this._watcher.push(cb)
    }

    validName(name) {
        return name.match(/^[a-zA-Z\._\-0-9]+$/) != null
    }

    validPath(name) {
        return name.match(/^\/[a-zA-Z\._\-0-9\/]*$/) != null
    }

    normalize(name) {
        while (name.indexOf('//') != -1) {
            name = name.replace('//', '/')
        }
        return name
    }

    async mkdir(name) {

        name = this.normalize(name)

        if (!this.validPath(name)) { throw `Invalid Path: ${name}`}
        if (name == "/") { throw "Cannot create root" }

        let existsAlready = await this.exists(name)
        if (existsAlready) { throw "Dir exists already" }

        let parent = this.pathname(name)
        let parentExists = await this.isDir(parent)
        if (!parentExists) { throw "Parent is not a directory" }


        await this._setDirinfo({path: name, files: []})

        var dirinfo = await this._getDirinfo(parent)
        dirinfo.files.push(name)
        dirinfo.files = dirinfo.files.sort()
        await this._setDirinfo(dirinfo)

        this._sendUpdate('mkdir', name)

    }

    async list(name) {

        name = this.normalize(name)

        if (!this.validPath(name)) { throw `Invalid Path: ${name}`}

        let exists = await this.isDir(name)
        if (!exists) { throw "Not a directory" }

        let dirinfo = await this._getDirinfo(name)

        let res = {}
        dirinfo.files.forEach(file => {
            res[this.basename(file)] = file
        })
        return res

    }

    async rmdir(name) {

        name = this.normalize(name)

        if (!this.validPath(name)) { throw `Invalid Path: ${name}`}
        if (name == "/") { throw "Cannot remove root" }

        let exists = await this.isDir(name)
        if (!exists) { throw "Not a directory" }

        let dirinfo = await this._getDirinfo(name)
        if (dirinfo.files.length != 0) { throw "Cannot delete non-empty directory" }

        let parent = this.pathname(name)
        let parentinfo = await this._getDirinfo(parent)
        parentinfo.files = parentinfo.files.filter(f => f != name)
        await this._setDirinfo(parentinfo)

        await this._deleteDirinfo(name)

        this._sendUpdate('rmdir', name)


    }

    async put(name, content) {

        name = this.normalize(name)

        if (!this.validPath(name)) { throw `Invalid Path: ${name}`}

        let isDir = await this.isDir(name)
        if (isDir) { throw "Not a file" }

        let isFile = await this.isFile(name)
        if (!isFile) {
            // does not exist yet; create
            await this._initFile(name)
        }

        await this._setFileinfo({path: name, content: content})

        this._sendUpdate('put', name)

    }

    async get(name) {

        name = this.normalize(name)

        if (!this.validPath(name)) { throw `Invalid Path: ${name}`}

        let exists = await this.isFile(name)
        if (!exists) { throw "Not a file" }

        let fileinfo = await this._getFileinfo(name)
        return fileinfo.content

    }

    async rm(name) {

        name = this.normalize(name)

        if (!this.validPath(name)) { throw `Invalid Path: ${name}`}

        let exists = await this.isFile(name)
        if (!exists) { throw "Not a file" }

        let parent = this.pathname(name)
        let isDir = await this.isDir(parent)
        if (!isDir) { throw "Parent not a directory" }

        let dirinfo = await this._getDirinfo(parent)
        dirinfo.files = dirinfo.files.filter(f => f != name)
        await this._setDirinfo(dirinfo)

        await this._deleteFileinfo(name)

        this._sendUpdate('rm', name)

    }

    async mv(oldname, newname) {

        oldname = this.normalize(oldname)
        newname = this.normalize(newname)

        if (!this.validPath(oldname)) { throw `Invalid Path: ${oldname}`}
        if (!this.validPath(newname)) { throw `Invalid Path: ${oldname}`}

        let exists = await this.isFile(oldname)
        if (!exists) { throw "Not a file" }

        let newExists = await this.exists(newname)
        if (newExists) { throw "Destination exists already" }

        let parent = this.pathname(oldname)
        let isDir = await this.isDir(parent)
        if (!isDir) { throw "Parent not a directory" }

        let newparent = this.pathname(newname)
        let isDirNew = await this.isDir(newparent)
        if (!isDir) { throw "New Parent not a directory"}

        let dirinfo = await this._getDirinfo(parent)
        dirinfo.files = dirinfo.files.filter(f => f != oldname)
        await this._setDirinfo(dirinfo)

        let newdirinfo = await this._getDirinfo(newparent)
        newdirinfo.files.push(newname)
        newdirinfo.files = newdirinfo.files.sort()
        await this._setDirinfo(newdirinfo)

        let fileinfo = await this._getFileinfo(oldname)
        fileinfo.path = newname
        await this._setFileinfo(fileinfo)

        await this._deleteFileinfo(oldname)

        this._sendUpdate('mv', {from: oldname, to: newname})

    }

    _getDirinfo(path) {

        return new Promise((resolve, reject) => {

            let transaction = this.db.transaction("dirs", "readonly")
            let os = transaction.objectStore("dirs")

            var req = os.get(path)
            req.onsuccess = _ => {
                resolve(req.result)
            }
            req.onerror = reject

        })

    }

    _setDirinfo(dirinfo) {

        return new Promise((resolve, reject) => {

            let transaction = this.db.transaction("dirs", "readwrite")
            let os = transaction.objectStore("dirs")

            let req = os.put(dirinfo)
            req.onsuccess = resolve
            req.onerror = reject

        })

    }

    _deleteDirinfo(path) {

        return new Promise((resolve, reject) => {

            let transaction = this.db.transaction("dirs", "readwrite")
            let os = transaction.objectStore("dirs")

            let req = os.delete(path)
            req.onsuccess = resolve
            req.onerror = reject

        })

    }

    _getFileinfo(path) {

        return new Promise((resolve, reject) => {

            let transaction = this.db.transaction("files", "readonly")
            let os = transaction.objectStore("files")

            var req = os.get(path)
            req.onsuccess = _ => {
                resolve(req.result)
            }
            req.onerror = reject

        })

    }

    _setFileinfo(fileinfo) {

        return new Promise((resolve, reject) => {

            let transaction = this.db.transaction("files", "readwrite")
            let os = transaction.objectStore("files")

            let req = os.put(fileinfo)
            req.onsuccess = resolve
            req.onerror = reject

        })

    }

    _deleteFileinfo(path) {

        return new Promise((resolve, reject) => {

            let transaction = this.db.transaction("files", "readwrite")
            let os = transaction.objectStore("files")

            let req = os.delete(path)
            req.onsuccess = resolve
            req.onerror = reject

        })

    }

    async _initFile(path) {

        let parent = this.pathname(path)
        let exists = await this.isDir(parent)
        if (!exists) { throw "Parent not a directory" }

        let dirinfo = await this._getDirinfo(parent)
        dirinfo.files.push(path)
        dirinfo.files = dirinfo.files.sort()
        await this._setDirinfo(dirinfo)

    }

    isDir(name) {

        return new Promise((resolve, reject) => {

            var req = this.db.transaction("dirs", "readwrite").objectStore("dirs").get(name)
            req.onsuccess = _ => resolve(req.result != null)
            req.onerror = reject

        })

    }

    isFile(name) {

        return new Promise((resolve, reject) => {

            var req = this.db.transaction("files", "readwrite").objectStore("files").get(name)
            req.onsuccess = _ => resolve(req.result != null)
            req.onerror = reject

        })

    }

    exists(name) {

        return new Promise((resolve, reject) => {

            this.isDir(name).then(exists => {
                if (exists) {
                    resolve(true)
                } else {
                    this.isFile(name).then(exists => {
                        resolve(exists)
                    }, reject)
                }
            }, reject)

        })

    }

    basename(name) {
        let fragments = name.split('/')
        return fragments[fragments.length - 1]
    }

    pathname(name) {
        let fragments = name.split('/')
        let path = fragments.slice(0, fragments.length - 1).join('/')
        return path == ""
            ? "/"
            : path
    }

    extension(name) {
        let basename = this.basename(name)
        let fragments = basename.split('.')
        return fragments[fragments.length - 1]
    }

    relative(name, base) {

        if (name.substr(0, base.length) != base) {
            return null
        }

        return name.substr(base.length + 1)

    }

    async download(name, displayName = null) {

        const contents = await this.get(name)

        const blob = new Blob([contents], {type: "text/plain"})
        const href = URL.createObjectURL(blob)
        const link = document.createElement('a')
        link.download = displayName ?? this.basename(name)
        link.href = href
        link.addEventListener("click", _ => {
            setTimeout(_ => {
                URL.revokeObjectURL(href)
            }, 150)
        })
        link.click()

    }

    async downloadBinary(name, displayName = null) {

        const blob = await this.get(name)
        const href = URL.createObjectURL(blob)
        const link = document.createElement('a')
        link.download = displayName ?? this.basename(name)
        link.href = href
        link.addEventListener("click", _ => {
            setTimeout(_ => {
                URL.revokeObjectURL(href)
            }, 150)
        })
        link.click()

    }

}

export default new LFS()