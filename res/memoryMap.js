export default class MemoryMap {

    static fromText(text) {

        if (text == null) { return {} }

        let memory = {}

        text.split("\n").forEach(line => {

            let match = line.match(/^0x([0-9a-f]+):?\s+0x([0-9a-f]+)\s*(;.*)?/)
            if (match) {
                const [_, addr, value] = match
                memory[this.toInt(addr)] = this.toInt(value ?? "")
            }

        })

        return memory

    }

    static toInt(hex) {

        return parseInt(hex, 16)

    }

    static toText(memory, symbols = []) {

        let lines = []

        const getSymbol = address => {
            return symbols.filter(s => s.address == address)[0]
        }
        
        for (const address of Object.keys(memory)) {
            const addr = ("000000" + parseInt(address).toString(16)).substr(-6)
            const value = ("000000" + parseInt(memory[address]).toString(16)).substr(-6)
            const symbol = getSymbol(address)
            if (symbol) {
                lines.push(`0x${addr} 0x${value} ;${symbol.name}`)
            } else {
                lines.push(`0x${addr} 0x${value}`)
            }
        }

        return lines.join("\n")

    }

}