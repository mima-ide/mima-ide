import Make from "../assembler/make.js"
import lfs from "../lfs.js"

export default class Analysis {

    static async analyzeFile(filename, architecture) {
        const make = new Make(architecture)
    
        try {
            await make.make(filename, Make.LINT)
        } catch (ex) {
    
            if (ex.lineInfo.macro) {
                return [ex, {
                    text: ex.text,
                    type: ex.type,
                    lineInfo: ex.lineInfo.macro
                }]
            } else {
                return [ex]
            }
        }
        
        return []
    }

    static async analyzeImports(filename) {

        const path = lfs.pathname(filename)
        const contents = await lfs.get(filename)
        const importsRegex = /^%import (.*)$/
        const lines = contents.split("\n")

        let lints = []

        for (let i = 0; i < lines.length; ++i) {
            const line = lines[i]
            const match = importsRegex.exec(line)
            if (!match) continue;

            lints.push({
                type: "import",
                text: `Import ${match[1]}`,
                import: `${path}/${match[1]}`,
                lineInfo: {
                    line: i,
                    file: filename
                }
            })
        }

        return lints
        

    }

    static async analyzeVariables(filename) {

        const contents = await lfs.get(filename)
        const variableRegex = /^([\w_][\w\d_]*)(:|\s)\s*DS\s*/
        const subroutineRegex = /^([\w_][\w\d_]*)(:|\s)\s*.sub\s*/
        const lines = contents.split("\n")

        let lints = []

        for (let i = 0; i < lines.length; ++i) {
            const line = lines[i]
            const variableMatch = variableRegex.exec(line)
            const subroutineMatch = subroutineRegex.exec(line)

            if (variableMatch) {
                lints.push({
                    type: 'variable',
                    text: `Variable ${variableMatch[1]}`,
                    variable: variableMatch[1],
                    lineInfo: {line: i, file: filename}
                })
            } else if (subroutineMatch) {
                lints.push({
                    type: 'subroutine',
                    text: `Subroutine ${subroutineMatch[1]}`,
                    subroutine: subroutineMatch[1],
                    lineInfo: {line: i, file: filename}
                })
            }

        }

        return lints

    }

    static async analyzeMacros(filename) {

        const contents = await lfs.get(filename)
        const macroRegex = /%([a-zA-Z0-9]+):?\s([a-zA-Z0-9\.\s]*)\s*(\[[a-zA-Z0-9\.\s=<>!$,]*\])?\s*{/
        const macroCallRegex = /%([a-zA-Z0-9]+)([a-zA-Z0-9\$\._\-\s]*)/
        const endRegex = /^\s*}\s*$/
        const lines = contents.split("\n")

        let lints = []
        let inMacro = false

        for (let i = 0; i < lines.length; ++i) {
            const line = lines[i]

            const macroDefinitionMatch = macroRegex.exec(line)

            if (macroDefinitionMatch) {

                lints.push({
                    type: "macro",
                    text: `Macro ${macroDefinitionMatch[1]}`,
                    macro: {
                        name: macroDefinitionMatch[1],
                        params: macroDefinitionMatch[2]
                    },
                    lineInfo: {
                        line: i,
                        file: filename
                    }
                })
                inMacro = true

                continue

            }

            const macroInvocationMatch = macroCallRegex.exec(line)
            if (macroInvocationMatch) {

                if (macroInvocationMatch[1] != "import") {

                    lints.push({
                        type: "macro-invocation",
                        text: `Use macro ${macroInvocationMatch[1]}`,
                        macro: {
                            name: macroInvocationMatch[1],
                            params: macroInvocationMatch[2]
                        },
                        lineInfo: {
                            line: i,
                            file: filename
                        }
                    })
                }

                continue

            }

            if (endRegex.exec(line)) {
                if (inMacro == false) {
                    lints.push({
                        type: "error",
                        text: `There is no macro here to end`,
                        lineInfo: {line: i, file: filename}
                    })
                } else {
                    inMacro = false
                }
            }
            
        }

        if (inMacro) {
            lints.push({
                type: "error",
                text: `Macro definition not closed`,
                lineInfo: {line: lines.length - 1, file: filename}
            })
        }

        return lints

    }

}