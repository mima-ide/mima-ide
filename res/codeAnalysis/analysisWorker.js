import Analysis from "./analysis.js"

addEventListener('message', e => {

    const {action, ticket} = e.data

    if (action == "analyze") {
        Analysis.analyzeFile(e.data.file, e.data.architecture).then(dataStep1 => {
            Analysis.analyzeImports(e.data.file).then(dataStep2 => {
                Analysis.analyzeMacros(e.data.file).then(dataStep3 => {
                    Analysis.analyzeVariables(e.data.file).then(dataStep4 => {
                        sendResult(ticket, dataStep1.concat(dataStep2).concat(dataStep3).concat(dataStep4))
                    })
                    
                })
                
            })
            
        })
    }

})

function sendResult(ticket, data) {

    postMessage({
        action: "result",
        ticket,
        result: data
    })

}
