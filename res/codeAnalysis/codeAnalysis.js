import lfs from "../lfs.js";
import Analysis from "./analysis.js";

export default class Linter {

    constructor() {

        this.ticket = 0
        this.callbacks = {}
        this.worker = new Worker("res/codeAnalysis/analysisWorker.js", {type: 'module'})
        this.worker.addEventListener('message', e => {
            const {action, ticket} = e.data
            if (action == "result") {
                this.callbacks[ticket](e.data.result)
            }
        })
        this.worker.addEventListener('error', e => {
            this.worker = null
        })

    }

    async lintFile(architecture, filename) {

        if (lfs.extension(filename) != "mima") return []

        return await this.scheduleAnalysis(architecture, filename)


    }

    scheduleAnalysis(architecture, filename) {
        return new Promise((resolve, reject) => {

            if (this.worker != null) {
                const t = ++this.ticket
                this.worker.postMessage({
                    action: 'analyze',
                    file: filename,
                    architecture,
                    ticket: t
                })

                this.callbacks[t] = resolve
            } else {
                Analysis.analyzeFile(filename, architecture).then(resolve, e => {
                    resolve([])
                })
            }

        })
        
    }

}