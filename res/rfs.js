class RFS {

    constructor() {
        this._dirmaps = {}
    }

    async _loadMap(url) {

        const response = await fetch(`${url}/_map.json`)

        if (response.ok) {
            const json = await response.json()
            this._dirmaps[url] = json
            return json
        } else {
            throw "Not a directory"
        }

    }

    async list(name) {

        let map = this._dirmaps[name] ?? (await this._loadMap(name))

        let res = {}
        map.forEach(file => {
            res[file.name] = `${name}/${file.name}`
        })
        return res

    }

    async get(name) {

        const response = await fetch(name)

        if (response.ok) {
            return response.text()
        } else {
            throw "Not found"
        }

    }

    async _dirmap(dir) {
        if (this._dirmaps[dir] != null) {
            return this._dirmaps[dir]
        } else {
            return await this._loadMap(dir)
        }
    }

    async isFile(name) {

        try {
            const dirname = this.pathname(name)
            const basename = this.basename(name)
            const dirmap = await this._dirmap(dirname)

            if (dirmap.filter(i => i.name == basename).length == 0) {
                return false
            }

            return dirmap.filter(i => i.name == basename)[0].type == "file"

        } catch(_) {
            return false
        }

    }

    async isDir(name) {

        try {
            const dirname = this.pathname(name)
            const basename = this.basename(name)
            const dirmap = await this._dirmap(dirname)

            if (dirmap.filter(i => i.name == basename).length == 0) {
                return false
            }

            return dirmap.filter(i => i.name == basename)[0].type == "dir"

        } catch(_) {
            return false
        }

    }


    basename(name) {
        let fragments = name.split('/')
        return fragments[fragments.length - 1]
    }

    pathname(name) {
        let fragments = name.split('/')
        let path = fragments.slice(0, fragments.length - 1).join('/')
        return path == ""
            ? "/"
            : path
    }

    extension(name) {
        let basename = this.basename(name)
        let fragments = basename.split('.')
        return fragments[fragments.length - 1]
    }

    relative(name, base) {
        if (name.substr(0, base.length) != base) {
            return null
        }
        return name.substr(base.length + 1)
    }

}

export default new RFS()