import menu from '../../ui/menu.js'
import microcode from '../microcode.js'
import codeHint from '../../ui/codeHint.js'

export default class MimamicroHint {

    attach(doc) {
        doc.on('cursorActivity', this.cursorActivity)
    }

    detach(doc) {
        codeHint.close(doc.cm.getWrapperElement())
    }

    sleep(doc) {
        codeHint.close(doc.cm.getWrapperElement())
    }

    awake(doc) {

    }

    cursorActivity(evt) {
        // const pos = evt.cm.cursorCoords(true, "page")
        const {line} = evt.cm.getCursor()
        const pos = evt.cm.charCoords({line, ch: 0}, "page")
        const data = evt.cm.getLine(line)

        const code = microcode.parseMemoryLine(data)

        if (code == null) {
            codeHint.close(evt.cm.getWrapperElement())
            return
        }

        const description = microcode.signalDescription(microcode.signalsFromMicrocode(code))

        codeHint.open(evt.cm.getWrapperElement(), 
            'ui-code-hint-microcode', 
            {description, code}, 
            {
                left: pos.left,
                top: pos.top + 20
            }
        )


    }

}