
var architectures = {

    mima: {
        argCodes: {
            0x0: "LDC",
            0x1: "LDV",
            0x2: "STV",
            0x3: "ADD",
            0x4: "AND",
            0x5: "OR",
            0x6: "XOR",
            0x7: "EQL",
            0x8: "JMP",
            0x9: "JMN",
            0xa: "LDIV",
            0xb: "STIV",
            0xc: "JMS",
            0xd: "JIND",
        },
        noargCodes: {
            0x0: "HALT",
            0x1: "NOT",
            0x2: "RAR",
        }
    },

    micromima: {
        argCodes: {
            0x0: "LDC",
            0x1: "LDV",
            0x2: "STV",
            0x3: "ADD",
            0x4: "AND",
            0x5: "OR",
            0x6: "XOR",
            0x7: "EQL",
            0x8: "JMP",
            0x9: "JMN",
            0xa: "LDIV",
            0xb: "STIV",
            0xc: "JMS",
            0xd: "JIND",
            0xe: "OPE",
        },
        noargCodes: {
            0x0: "HALT",
            0x1: "NOT",
            0x2: "RAR",
            0x3: "OPF3",
            0x4: "OPF4",
            0x5: "OPF5",
            0x6: "OPF6",
            0x7: "OPF7",
            0x8: "OPF8",
            0x9: "OPF9",
            0xa: "OPFA",
            0xb: "OPFB",
            0xc: "OPFC",
            0xd: "OPFD",
            0xe: "OPFE",
            0xf: "OPFF",
        }
    },

    mimax: {
        argCodes: {
            0x0: "LDC",
            0x1: "LDV",
            0x2: "STV",
            0x3: "ADD",
            0x4: "AND",
            0x5: "OR",
            0x6: "XOR",
            0x7: "EQL",
            0x8: "JMP",
            0x9: "JMN",
            0xa: "LDIV",
            0xb: "STIV",
            0xc: "CALL",
            0xd: "LDVR",
            0xe: "STVR",    
        },
        noargCodes: {
            0x0: "HALT",
            0x1: "NOT",
            0x2: "RAR",
            0x3: "RET",
            0x4: "LDSP",
            0x5: "STSP",
            0x6: "LDFP",
            0x7: "STFP",
            0x8: "LDRA",
            0x9: "STRA",
            0xa: "ADC",
            0xb: "OPFB",
            0xc: "OPFC",
            0xd: "OPFD",
            0xe: "OPFE",
            0xf: "OPFF",    
        }
    }

}

export default class Opcodes {

    static registerArchitecture(arch, argCodes, noargCodes) {

        architectures[arch] = {
            argCodes, noargCodes
        }

    }

    static listMnemonics(arch = 'mima') {

        let codes = architectures[arch]

        return Object.keys(codes.argCodes).map(c => codes.argCodes[c]).concat(
            Object.keys(codes.noargCodes).map(c => codes.noargCodes[c])
        ) 

    }

    static getArgCodes(arch = 'mima') {

        return architectures[arch].argCodes

    }

    static getNoargCodes(arch = 'mima') {

        return architectures[arch].noargCodes

    }

    static getOpcode(mnemonic, arch = 'mima') {

        mnemonic = mnemonic.toUpperCase()

        for (const _argCode of Object.keys(architectures[arch].argCodes)) {
            if (architectures[arch].argCodes[_argCode] == mnemonic) {
                return parseInt(_argCode) << 20
            }
        }

        for (const _noargCode of Object.keys(architectures[arch].noargCodes)) {
            if (architectures[arch].noargCodes[_noargCode] == mnemonic) {
                return 0xf00000 | parseInt(_noargCode) << 16
            }
        }

        throw `Mnemonic ${mnemonic} not in instruction set for ${arch}`

    }

    static getMnemonic(opcode, symbolsReverse, arch = 'mima') {

        symbolsReverse = symbolsReverse ?? {}

		const op1 = (opcode & 0xf00000) >> 20;
		const op2 = (opcode & 0x0f0000) >> 16;
        const argument1 = opcode & 0x0fffff;
        const argument2 = opcode & 0x00ffff;

        const argument = op1 == 0xf 
            ? argument2 
            : argument1
        const hexArgument = argument.toString(16)

        let mnemonic = op1 == 0xf 
            ? architectures[arch].noargCodes[op2]
            : architectures[arch].argCodes[op1]

        mnemonic = (mnemonic + "     ").substr(0, 5)

        const symbol = symbolsReverse[argument]

        if (mnemonic == "undef") {
            return `$${opcode.toString(16)}`
        } else if (op1 == 0x0 && op2 == 0x0) {
            return `${mnemonic} ${argument}`
        } else if (op1 == 0xf && argument == 0) {
            return `${mnemonic}`
        } else if (symbol != null) {
            return `${mnemonic} ${symbol}`
        } else {
            return `${mnemonic} $${hexArgument}`
        }

    }

}