import lfs from "../lfs.js"
import Preprocessor from "./preprocessor.js"
import Merger from "./merger.js"
import Linker from "./linker.js"
import Assembler from "./assembler.js"
import Bytecode from "./bytecode.js"

class Make {

    constructor(architecture = 'mima', simulator = null) {
        this.architecture = architecture
        this.simulator = simulator
    }

    async make(file, action = Make.BUILD, fileCallback = title => {}) {

        let {text: sourceCode, lineMap: sourceLineMap} = await new Merger().process(file)
        let {text: preprocessedCode, lineMap: preprocessedLineMap} = await new Preprocessor(this.architecture).process(sourceCode, sourceLineMap)
        if (action == Make.PREPROCESS) {
            await this.writeData(preprocessedCode, fileCallback, "Preprocessed File")
            return
        }

        let linker = new Linker(this.architecture)
        let {text: linkedCode, lineMap: linkedLineMap} = await linker.process(preprocessedCode, preprocessedLineMap)
        if (action == Make.LINK) {
            await this.writeData(linkedCode, fileCallback, "Linked File")
            return
        }

        let {text: memoryMap, lineMap: memoryLineMap} = await new Assembler(this.architecture).process(linkedCode, linker.symbolMap, linkedLineMap)
        if (action == Make.BUILD) {
            await this.writeData(memoryMap, fileCallback, "Memory Map")
            return
        } else if (action == Make.LINT) {
            return
        }

        if (action == Make.BUILD_FLASH) {
            this.simulator.flash(memoryMap, linker.symbolMap, linker.segmentMap, this.startAddress(linker.symbolMap), memoryLineMap)
        } else if (action == Make.BYTECODE) {
            await this.generateBytecode(memoryMap, this.startAddress(linker.symbolMap), fileCallback)
        } else if (action == Make.SYMBOL_MAP) {
            await this.generateSymbolMap(linker.symbolMap, linker.segmentMap, fileCallback)
        }

    }

    async writeData(data, fileCallback, title) {

        const filename = await fileCallback(title)
        await lfs.put(filename, data)

    }

    async generateBytecode(memoryMap, startAddress, fileCallback) {

        const bytecoder = new Bytecode()
        const blob = await bytecoder.process(memoryMap, startAddress)
        
        let filename = await fileCallback("Bytecode")
        if (lfs.extension(filename) != 'bin') { filename += '.bin' }

        await lfs.put(filename, blob)

    }

    async generateSymbolMap(symbolMap, segmentMap, fileCallback) {

        let symbols = symbolMap.map(s => `.symbol ${s.name} ${s.address}`).join("\n")
        let segments = segmentMap.map(s => `* = ${s.location}\n.size ${s.size}\n.${s.type}`).join("\n")

        const filename = await fileCallback("Symbol Map")
        await lfs.put(filename, symbols + "\n" + segments)

    }

    startAddress(symbols) {


        const startEntry = symbols.filter(s => s.name == "START")[0]
        if (startEntry != null) {
            return startEntry.address
        }

        const mainEntry = symbols.filter(s => s.name == "MAIN")[0]
        if (mainEntry != null) {
            return mainEntry.address
        }

        return 0

    }



}

Make.BUILD = 'build'
Make.BUILD_FLASH = 'build+flash'
Make.PREPROCESS = 'preprocess'
Make.LINK = 'link'
Make.BYTECODE = 'bytecode'
Make.SYMBOL_MAP = 'symbol_map'
Make.LINT = 'lint'

export default Make