import MemoryMap from "../memoryMap.js"
import op from "./opcodes.js"

export default class Assembler {

    constructor(architecture) {

        this.architecture = architecture
        let opcodeList = op.listMnemonics(architecture)
                            .concat(["DS"])
                            .map(m => [m.toUpperCase(), m.toLowerCase()])
                            .flat()
                            .sort((a,b) => a.length - b.length)
                            .reverse()
        let opcodeRegex = opcodeList.join("|")

        let symbolRegex = '([\\w_][\\w\\d_]*)(:|\\s)'
        let numberRegex = '(\\$[\\da-fA-F]+|\\-?[\\d]+)'
        let labelRegex = '[\\w_][\\w\\d_]*'
        let relativeRegex = '\\-?\\d+\\(IP\\)|\\-?\\d+\\(IAR\\)'
        this.instructionRegex = new RegExp(`^(${symbolRegex})?\\s*(${opcodeRegex})\\s*((${relativeRegex})|${numberRegex}|(${labelRegex}))?\\s*(;.*)?$`)

        this.IDX_FRONT  = 1
        this.IDX_SYMBOL = 2
        this.IDX_OPCODE = 4
        this.IDX_NUMBER = 7
        this.IDX_RELATIVE = 6
        this.IDX_LABEL  = 8
        this.IDX_COMMENT = 9


    }

    async process(data, symbolMap = [], inMap = []) {

        let lines = data.split("\n").filter(l => l.trim() != "")

        let {memory, lineMap} = this.assemble(lines, inMap)

        return {text: MemoryMap.toText(memory, symbolMap), lineMap}

    }

    assemble(lines, inMap) {

        // lines = lines.filter(l => !l.match(/;.*/))
        let lineMap = []
        let memory = {}
        let address = 0

        for (let i = 0; i < lines.length; ++i) {

            const line = lines[i].trim()

            if (line.match(/;.*/)) continue;
          
            const addressMatch = /\*\s+=\s+(\$?\d+)/.exec(line)

            if (addressMatch) {
                address = this.parseAddress(addressMatch[1])
                continue
            }

            if (line.trim() == "") { // empty line
                continue
            }

            const match = this.instructionRegex.exec(line)
            if (!match) { continue }

            const mnemonic = match[this.IDX_OPCODE]
            const value = this.parseAddress(match[this.IDX_NUMBER]) || 0

            if (mnemonic == "DS" || mnemonic == "ds") {
                memory[address] = value & 0xffffff
            } else {
                const opcode = op.getOpcode(mnemonic, this.architecture)
                const hexcode = opcode | (value & 0x0fffff)

                memory[address] = hexcode
            }

            lineMap[address] = inMap[i]
            

            ++address

        }

        return {memory, lineMap}


    }

    parseAddress(address) {

        if (address == null || address.trim() == "") { return 0 }

        if (address.substr(0,1) == "$") {
            return parseInt(address.substr(1).trim(), 16)
        } else {
            return parseInt(address.trim())
        }

    }

}