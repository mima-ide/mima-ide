export default class Preprocessor {
    
    MACRO_DEF_REGEX = /^%([a-zA-Z][a-zA-Z0-9]*):?\s+((\.[a-zA-Z][a-zA-Z0-9]*\s+)*)?(\[([a-zA-Z0-9\.\s=!<>,]*)\])?\s*{\s*$/
    MACRO_EXPANSION_REGEX = /^(([a-zA-Z_][a-zA-Z0-9_]*):?\s+)?%([a-zA-Z][a-zA-Z0-9]*)(\s([a-zA-Z0-9\$\-\s]*))?(;.*)?$/

    constructor(arch) {
        this.architecture = arch
    }

    async process(data, map = []) {
        const lines = data.split("\n")
        // 1. Find all macros
        const macros = this.findMacros(lines, map)

        // 2. Expand macros
        const {lines: newLines, map: newMap} = this.processFile(lines, macros, map)

        return {text: newLines.join('\n'), lineMap: newMap}
    }

    findMacros(lines, map) {

        let macros = []
        let macroLines = []
        let currentMacro = null

        for (let i in lines) {

            const line = lines[i]
            const trimmedLine = line.trim()

            if (currentMacro != null) {
                if (trimmedLine == "}") {
                    // end of macro definition. Push macro to macro list
                    currentMacro.lines = macroLines
                    currentMacro.end = map[i]
                    macros.push(currentMacro)
                    currentMacro = null
                } else if (trimmedLine.match(this.MACRO_DEF_REGEX)) {
                    throw {
                        lineInfo: map[i],
                        text: "Macro definition not allowed here",
                        type: "error"
                    }
                } else {
                    macroLines.push(line)
                }
            } else {
                let match = trimmedLine.match(this.MACRO_DEF_REGEX)
                if (trimmedLine == "}") {
                    throw {
                        lineInfo: map[i],
                        text: "There is no macro here to end",
                        type: "error"
                    }
                } else if (match != null) {
                    const [fullMatch, name, params, _, _2, conditions] = match
                    currentMacro = {
                        name, 
                        parameters: (params != null) ? params.split(/\s/).filter(s => s != "") : [], 
                        conditions: (conditions != null) ? this.parseConditions(conditions) : [],
                        lines: [],
                        start: map[i],
                        end: map[i]
                    }
                    macroLines = []
                }
            }

            const match = line.match(this.MACRO_DEF_REGEX)
        }

        return macros

    }

    getMacro(name, args, macros) {

        return macros.filter(m => m.name == name)
                     .sort((a, b) => b.conditions.length - a.conditions.length)
                     .filter(m => this.checkConditions(m, args))
                     [0]

    }

    parseConditions(conditions) {

        let list = []
        const singleConds = conditions.split(',')
        const condRegex = /^([\.a-zA-Z0-9\$]+)\s+(!=|<|<=|>=|>|=)\s+([\.a-zA-Z0-9\$]+)$/

        for (let cond of singleConds) {
            let match = condRegex.exec(cond.trim())
            if (match) {
                const [_, lhs, op, rhs] = match

                list.push({lhs, op, rhs})
            }
        }

        return list

    }

    checkConditions(macro, args) {

        if (macro.conditions.length == 0) return true

        const evaluate = expr => {
            if (expr == 'arch') { // architecture
                return this.architecture
            } else if (expr.match(/^\.[a-zA-Z0-9]$/)) { // macro variable
                const index = macro.parameters.indexOf(expr)
                return (index >= 0) ? args[index] : false
            } else if (expr.match(/^\$[0-9a-f]+$/)) { // hex number
                return parseInt(expr.substr(1).trim(), 16)
            } else if (expr.match(/^[0-9]+$/)) { // number
                return parseInt(expr.trim())
            } else {
                return expr
            }
        }

        const operations = {
            '=': (a, b) => a == b,
            '<': (a, b) => a < b,
            '<=': (a,b) => a <= b,
            '>': (a, b) => a > b,
            '>=': (a,b) => a >= b,
            '!=': (a,b) => a != b,
        }

        for (let cond of macro.conditions) {

            const lhs = evaluate(cond.lhs)
            const rhs = evaluate(cond.rhs)
            const op = operations[cond.op]

            if (op == null) {
                throw `Condition ${lhs} ${cond.op} ${rhs} not supported`
            }

            if (!op(lhs, rhs)) return false

        }

        return true

    }

    processFile(lines, macros, map) {

        let outputLines = []
        let outputMap = []
        let insideDefinition = false

        for (let i in lines) {
            const line = lines[i]
            const trimmedLine = line.trim()
             
            if (line == "}" && insideDefinition) {
                insideDefinition = false
                continue
            } else if (trimmedLine.match(this.MACRO_DEF_REGEX) && !insideDefinition) { // skip macro definitions
                insideDefinition = true
                continue
            } else if (insideDefinition) {
                continue
            }

            outputLines.push(line)
            outputMap.push(map[i])

        }

        // outputLines only has lines in global scope
        return this.expandMacros(outputLines, macros, outputMap, [])

    }

    expandMacros(lines, macros, map, forbiddenMacros = []) {


        let outputLines = []
        let outputMap = []
        for (let i in lines) {
            const line = lines[i]
            const trimmedLine = line.trim()
            const match = trimmedLine.match(this.MACRO_EXPANSION_REGEX)

            if (match) {

                const randomID = '_' + Math.random().toString(36).substr(2, 9).toUpperCase()
                // macro expansion
                const [fullMatch, _, label, macroName, _2, args, comment] = match
                const parsedArgs = (args != null) ? args.split(/\s/).filter(s => s != '') : []

                if (forbiddenMacros.includes(macroName)) {
                    throw {
                        lineInfo: map[i],
                        text: `Recursive exapnsion of macro ${macroName} is forbidden`,
                        type: 'error'
                    }
                }

                const macro = this.getMacro(macroName, parsedArgs, macros)

                if (macro == null) {
                    throw {
                        lineInfo: map[i],
                        text: `No macro with name ${macroName} for arguments ${parsedArgs} found`,
                        type: "error"
                    }
                }

                
                let macroLines = macro.lines
                let macroMap = []
                for (let j = 0; j < macro.lines.length; ++j) {
                    macroMap.push({file: macro.start.file, line: macro.start.line + j + 1})
                }
                // replace parameters with arguments
                for (let j in macro.parameters) {
                    macroLines = macroLines.map(l => l.replace(macro.parameters[j], parsedArgs[j]))
                }
                // replace magic values
                macroLines = macroLines.map(l => l.replace('.macro', macroName))
                macroLines = macroLines.map(l => l.replace('.~', randomID))
                
                let {lines: recvMacroLines, map: recvMacroMap} = this.expandMacros(macroLines, macros, macroMap, forbiddenMacros.concat(macroName))

                for (let j = 0; j < recvMacroLines.length; ++j) {
                    outputLines.push(recvMacroLines[j])
                    outputMap.push({
                        file: map[i].file,
                        line: map[i].line,
                        origin: recvMacroMap[j]
                    })
                }




            } else {

                // .sub and .ret compiler intrinsics
                let processedLine = line.replace('.sub', "DS 0")
                                        .replace('.ret', "JIND")
                // simple line
                outputLines.push(processedLine)
                outputMap.push(map[i])
            }
        }

        return {lines: outputLines, map: outputMap}

    }

}