export default class Bytecode {

    async process(memoryMap, startAddress) {

        const lastAddress = Math.max(...Object.keys(memoryMap))

        const headerSize = 10
        const bufferSize = (lastAddress + 1) * 3 + headerSize

        let buffer = new ArrayBuffer(bufferSize)
        let byteBuffer = new Uint8Array(buffer)

        for (let i = 0; i <= lastAddress; ++i) {

            const code = memoryMap[i] ?? 0
            const c2 = (code >> 16) & 0xff
            const c1 = (code >> 8) & 0xff
            const c0 = code & 0xff

            byteBuffer[headerSize + 3 * i] = c2
            byteBuffer[headerSize + 3 * i + 1] = c1
            byteBuffer[headerSize + 3 * i + 2] = c0

        }

        byteBuffer[0] = 0x4d // M
        byteBuffer[1] = 0x49 // I
        byteBuffer[2] = 0x4d // M
        byteBuffer[3] = 0x41 // A
        byteBuffer[4] = 0x00 // \0

        byteBuffer[5] = (startAddress >> 16) & 0xff // Start address
        byteBuffer[6] = (startAddress >> 8) & 0xff
        byteBuffer[7] = (startAddress >> 0) & 0xff

        byteBuffer[8] = 0x00 // Input fields
        byteBuffer[9] = 0x00 // Output fields

        return new Blob([byteBuffer])


    }

}