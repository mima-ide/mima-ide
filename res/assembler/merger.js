import lfs from "../lfs.js"

export default class Merger {

    async process(filename) {

        let alreadyIncluded = [filename]
        let foundIncludes = await this.includesForFile(filename)

        while (foundIncludes.length != 0) {

            let nextInclude = foundIncludes.splice(0, 1)[0]

            alreadyIncluded.splice(0,0, nextInclude)
            let newIncludes = (await this.includesForFile(nextInclude))
                                .filter(i => !alreadyIncluded.includes(i))

            
            foundIncludes.splice(0,0,...newIncludes)

        }

        return await this.composeFile(alreadyIncluded)


    }

    async includesForFile(filename) {

        let path = lfs.pathname(filename)
        let contents = await lfs.get(filename)
        let lines = contents.split("\n")

        return lines.filter(l => l.match(/^%import/))
                    .map(l => l.match(/^%import (.*)$/)[1])
                    .map(f => `${path}/${f}`)


    }

    async composeFile(files) {

        let allLines = []
        let lineMap = []

        for (const f of files) {
                
            let fileContents = await lfs.get(f)
            let lines = fileContents.split("\n")
            for (let i = 0; i < lines.length; ++i) {
                if (!lines[i].match(/^%import/)) {
                    allLines.push(lines[i])
                    lineMap.push({file: f, line: i})
                }
            }

        }

        return {text: allLines.join("\n"), lineMap}

    }

}