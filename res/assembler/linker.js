import op from "./opcodes.js"

export default class Linker {

    constructor(architecture) {
        this.architecture = architecture
        let opcodeList = op.listMnemonics(architecture)
                            .concat(["DS"])
                            .map(m => [m.toUpperCase(), m.toLowerCase()])
                            .flat()
                            .sort((a,b) => a.length - b.length)
                            .reverse()
        let opcodeRegex = opcodeList.join("|")

        let symbolRegex = '([\\w_][\\w\\d_]*)(:|\\s)'
        let numberRegex = '(\\$[\\da-fA-F]+|\\-?[\\d]+)'
        let labelRegex = '[\\w_][\\w\\d_]*'
        let relativeRegex = '\\-?\\d+\\(IP\\)|\\-?\\d+\\(IAR\\)'
        this.instructionRegex = new RegExp(`^(${symbolRegex})?\\s*(${opcodeRegex})\\s*((${relativeRegex})|${numberRegex}|(${labelRegex}))?\\s*(;.*)?`)

        this.IDX_FRONT  = 1
        this.IDX_SYMBOL = 2
        this.IDX_OPCODE = 4
        this.IDX_NUMBER = 7
        this.IDX_RELATIVE = 6
        this.IDX_LABEL  = 8
        this.IDX_COMMENT = 9

        this.symbolMap = []
        this.segmentMap = []

    }

    async process(data, inMap = []) {

        let lines = data.split("\n")//.filter(l => l.trim() != "")

        let segments = this.findSegments(lines, inMap)

        let freeSegments = this.calculateFreeSegments(segments)
        segments = this.placeAutoSegments(segments, freeSegments)

        let symbols = this.calculateSymbolsForAllSegments(segments)

        segments = this.replaceSymbolsInAllSegments(segments, symbols)

        this.symbolMap = symbols
        this.segmentMap = segments.map(s => {
            return {
                location: s.location,
                size: s.size,
                type: s.type
            }
        })

        return this.mergeSegments(segments)

    }

    findSegments(lines, lineMap) {

        let segments = []
        let currentSegment = {
            location: 0,
            size: -1,
            lines: [],
            type: "",
            symbols: [],
            lineInfo: []
        }

        for (let i = 0; i < lines.length; ++i) {

            const line = lines[i]
            
            let matchesAddressDefinition = line.match(/\*\s+=\s+(\$?[0-9a-f]+)/)
            let matchesSegmentStarter = line.match(/^\s*\.auto/)
            let matchesDirective = line.match(/^\s*\.(size|fill|data|text|symbol)\s*(.*)/)

            if (line.trim() == "") {
                continue
            } else if (matchesAddressDefinition) {
                let address = this.parseAddress(matchesAddressDefinition[1])

                currentSegment.type = currentSegment.type != "" ? currentSegment.type : "data"
                segments.push(currentSegment)
                currentSegment = {
                    location: address,
                    size: -1,
                    lines: [],
                    type: "",
                    symbols: [],
                    lineInfo: []
                }
            } else if (matchesSegmentStarter) {
                currentSegment.type = currentSegment.type != "" ? currentSegment.type : "data"
                segments.push(currentSegment)
                currentSegment = {
                    location: -1,
                    size: -1,
                    lines: [],
                    type: "",
                    symbols: [],
                    lineInfo: []
                }
            } else if (matchesDirective) {
                
                let [_, directive, params] = matchesDirective
                let parameters = params.trim().split(" ").filter(e => e.trim() != "")
                if (directive == "size") {
                    currentSegment.size = this.parseAddress(parameters[0])
                } else if (directive == "fill") {
                    let value = parameters.slice(1).join(" ")
                    let times = this.parseAddress(parameters[0])
                    for (let j = 0; j < times; ++j) { 
                        currentSegment.lines.push(value) 
                        currentSegment.lineInfo.push(lineMap[i])
                    }
                } else if (directive == "data") {
                    currentSegment.type = "data"
                } else if (directive == "text") {
                    currentSegment.type = "text"
                } else if (directive == "symbol") {
                    let [symbolName, symbolAddress] = parameters
                    currentSegment.symbols.push({
                        name: symbolName,
                        address: this.parseAddress(symbolAddress)
                    })
                }


            } else {
                if (currentSegment.type == "") {
                    let instructionMatch = this.instructionRegex.exec(line)
                    if (instructionMatch) {
                        if (instructionMatch[this.IDX_OPCODE].toUpperCase() == "DS") {
                            currentSegment.type = "data"
                        } else {
                            currentSegment.type = "text"
                        }
                    }
                }
                currentSegment.lines.push(line)
                currentSegment.lineInfo.push(lineMap[i])
            }

        }

        segments.push(currentSegment)

        return this.recalculateSizes(segments)

    }

    parseAddress(address) {

        if (address.substr(0,1) == "$") {
            return parseInt(address.substr(1).trim(), 16)
        } else {
            return parseInt(address.trim())
        }

    }

    recalculateSizes(segments) {

        segments.forEach(segment => {
          
            let calculatedSize = segment.lines.filter(l => !l.match(/;.*/) && l.trim() != "" ).length
            segment.size = Math.max(segment.size, calculatedSize)

        })


        return segments.filter(s => s.size != 0)

    }

    calculateSymbolsForSegment(segment) {

        let lines = segment.lines.filter(l => l.trim() != "" && !l.match(/;.*/))
        let counter = 0
        let symbols = []

        lines.forEach(line => {

            line = line.trim()
            let match = this.instructionRegex.exec(line)
            if (!match) { return }

            if (match[2] != null) {

                let symbolLabel = match[2]
                symbols.push({
                    name: symbolLabel,
                    address: segment.location + counter
                })

            }

            ++counter

        })

        return symbols.concat(segment.symbols)

    }

    calculateSymbolsForAllSegments(segments) {

        return segments.map(s => this.calculateSymbolsForSegment(s)).flat()

    }

    replaceSymbolsInSegment(segment, symbols) {

        let newLines = []
        let count = 0
        for (let i = 0; i < segment.lines.length; ++i) {
            const line = segment.lines[i].trim()

            let instructionMatch = this.instructionRegex.exec(line)
            if (!instructionMatch) {
                newLines.push(line)
                continue
            }

            if (instructionMatch[this.IDX_LABEL] != null) {
                let labelName = instructionMatch[this.IDX_LABEL]
                let labelAddress = this.findSymbolAddress(labelName, symbols, segment.lineInfo[i])

                newLines.push(
                        (instructionMatch[this.IDX_FRONT] || " ")
                        + " " + instructionMatch[this.IDX_OPCODE]
                        + " " + labelAddress
                        + (instructionMatch[this.IDX_COMMENT] || "")
                )

                // newLines.push(line.replace(labelName, labelAddress))
            } else if(instructionMatch[this.IDX_RELATIVE] != null) {
                // relative to IP
                let offsetName = instructionMatch[this.IDX_RELATIVE]
                let offset = this.parseAddress(offsetName.split("(")[0])
                let offsetAddress = segment.location + count + offset

                newLines.push(line.replace(offsetName, offsetAddress))
            } else {
                newLines.push(line)
            }

            ++count

        }

        return newLines

    }

    replaceSymbolsInAllSegments(segments, symbols) {

        return segments.map(segment => {
            segment.lines = this.replaceSymbolsInSegment(segment, symbols)
            return segment
        })

    }

    findSymbolAddress(label, symbols, lineInfo) {

        let symbolInfo = symbols.filter(s => s.name == label)[0]
        if (symbolInfo == null) {
            throw {lineInfo, text: `Symbol ${label} not defined in line ${lineInfo.line} ${lineInfo.file}`, type: "undefined", symbol: label}
        }
        return symbolInfo.address

    }

    mergeSegments(segments) {

        const text = segments
                    .map(s => [`* = ${s.location}`].concat(s.lines))
                    .flat()
                    .join("\n")
        const lineMap = segments
                    .map(s => [{}].concat(s.lineInfo))
                    .flat()

        return {text, lineMap}

    }

    calculateFreeSegments(segments) {

        let freeSegments = [{location: 0, size: 0xffffff}]

        segments.forEach(segment => {
            
            if (segment.location == -1) { return }

            let overlappingSegment = freeSegments.filter(s => {
                return s.location <= segment.location && s.location + s.size >= segment.location
            })[0]

            if (overlappingSegment == null) { return }

            freeSegments = freeSegments.filter(s => overlappingSegment != s).concat([
                {location: overlappingSegment.location, size: segment.location - overlappingSegment.location},
                {location: segment.location + segment.size, size: overlappingSegment.location + overlappingSegment.size - segment.location - segment.size},
            ])

        })

        return freeSegments

    }

    placeAutoSegments(segments, freeSegments) {

        freeSegments = freeSegments.sort((a, b) => a.size - b.size)

        for (let _segment in segments) {

            if (segments[_segment].location == -1) {
                let placement = this.findAutoSegment(segments[_segment].size, freeSegments)
                segments[_segment].location = placement.location

                // recreate free list
                freeSegments = this.calculateFreeSegments(segments).sort((a, b) => a.size - b.size)
            }


        }

        return segments

    }

    findAutoSegment(size, freeSegments) {

        for (let segment of freeSegments) {

            if (size <= segment.size) {
                return segment
            }

        }

        throw {text: "Could not find a segment for auto-placement", type: "error", lineInfo: {file: "/", line: 0}}

    }

}