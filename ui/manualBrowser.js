import Vue from './vue.esm.js'
import rfs from '../res/rfs.js'
import findMenu from './findMenu.js'

Vue.component('ui-manual-browser', {
    template: `<span class="ui-manual-browser">
            <div class='mini-toolbar'>
            <ui-button
                icon="fas-home"
                tip="Manual Startpage"
                @click.native="goHome"
                ></ui-button>
            <ui-button
                icon='fas-arrow-left'
                tip='Back'
                @click.native="goBack"
                :class="{disabled: this.history.length <= 1}"
                ></ui-button>
            <ui-button
                icon='fas-arrow-right'
                tip='Forward'
                @click.native="goForward"
                :class="{disabled: this.future.length == 0}"
                ></ui-button>
            <div class='ui-div'></div>
            <ui-button 
                class="chapter-selector" 
                :title="currentChapterName"
                icon="fas-sort"
                @click.native="openChapterMenu"
                ></ui-button>

        </div>

        <div class='ui-scroll'>
            <ui-markdown-view :url="currentChapter" v-if="currentChapter != ''" @navigate="navigate"></ui-markdown-view>
        </div>

    </span>`,
    data: _ => {
        return {
            chapters: [],
            currentChapter: "",
            history: [],
            future: []
        }
    },
    props: ['base'],
    computed: {
        chapterMenu() {

            return this.chapters.map(file => {
                return {
                    id: file.path,
                    title: file.name,
                    callback: _ => {
                        this.currentChapter = file.path
                        this.history.push(file.path)
                        this.future = []
                    }
                }
            })

        },

        currentChapterName() {
            return this.chapterName(this.currentChapter)
        },
    },
    watch: {
    },
    methods: {

        async loadIndex(url) {
            const files = await rfs.list(url)
            Object.keys(files).forEach(async file => {
                const path = files[file]
                const isDir = await rfs.isDir(path)

                if (isDir) {
                    this.loadIndex(path)
                } else {
                    this.chapters.push({
                        name: this.chapterName(path),
                        path
                    })
                }
            })
        },

        openChapterMenu(evt) {
            findMenu.open(evt.target, this.chapterMenu)
        },

        chapterName(path) {

            path = rfs.relative(path, this.base)

            if (path == null || path == "index.md") {
                return "Manual"
            }

            return path.split('/')
                       .filter(i => i != "index.md")
                       .map(i => i.replace('.md', ''))
                       .map(i => i.replace('_', ' '))
                       .join(" > ")

        },

        navigate(url) {
            let indexHref = location.href
            if (indexHref.substr(-11) == "/index.html") {
                indexHref = indexHref.substr(0, indexHref.length -10)
            }

            if (url.substr(0, indexHref.length) == indexHref) {
                let path = url.substr(indexHref.length)
                let currentPath = rfs.pathname(this.currentChapter)
                this.currentChapter = `${currentPath}/${path}`
                this.history.push(`${currentPath}/${path}`)
                this.future = []
            } else {
                location.href = url
            }

        },

        goBack() {
            if (this.history.length <= 1) { return }
            this.history.pop()
            const path = this.history[this.history.length - 1]
            if (path != null) {
                this.future.push(this.currentChapter)
                this.currentChapter = path
                
            }
        },

        goForward() {
            if (this.future.length == 0) { return }
            const path = this.future.pop()
            if (path != null) {
                this.currentChapter = path
                this.history.push(path)
            }
        },

        goHome() {
            this.currentChapter = `${this.base}/index.md`
            this.history.push(this.currentChapter)
        },

    },

    mounted: function() {
        this.loadIndex(this.base)
        this.currentChapter = `${this.base}/index.md`
        this.history.push(this.currentChapter)
    }

})
