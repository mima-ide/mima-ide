import Vue from './vue.esm.js'

Vue.component('ui-spot', {
    template: '<span class="ui-spot" :style="[style]" />',
    props: ["x", "y"],
    computed: {
        style: function() {
            if (this.x == -1 || this.y == -1) {
                return {}
            }
            return {
                background: 'radial-gradient(circle at ' + this.x + 'px ' + this.y + 'px, var(--ui-spot-color) 0%, rgba(225,225,225,0) 100%)'
            }
        }
    },
})