export default {
    data: _ => {
        return {
            pointerX: 0,
            pointerY: 0
        }
    },
    computed: {
        parallax: function() {

            if (this.pointerX == -1 || this.pointerY == -1 || this.$el == undefined) { 
                return {
                    boxShadow: ""
                } 
            }

            let shadowX = Math.round(((this.$el.offsetWidth / 2) - this.pointerX) / 3)
            let shadowY = Math.round(((this.$el.offsetHeight / 2) - this.pointerY) / 3)

            if (shadowX > 4) shadowX = 4
            if (shadowX < -4) shadowX = -4
            if (shadowY > 4) shadowY = 4
            if (shadowY < -4) shadowY = -4

            return {
                boxShadow: shadowX + "px " + shadowY + "px 6px var(--ui-shadow-color)"
            }
        }
    },
    mounted: function() {

        this.$el.addEventListener('mousemove', evt => {

            const position = {
                x: evt.pageX,
                y: evt.pageY
              };
            
              const offset = {
                left: this.$el.offsetLeft,
                top: this.$el.offsetTop
              };
            
              let reference = this.$el.offsetParent;
            
              while(reference){
                offset.left += reference.offsetLeft;
                offset.top += reference.offsetTop;
                reference = reference.offsetParent;
              }

              this.pointerX = position.x - offset.left
              this.pointerY = position.y - offset.top
            

        })

        this.$el.addEventListener('mouseout', evt => {
            this.pointerX = -1
            this.pointerY = -1
        })
    }
}