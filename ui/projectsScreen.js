import Vue from './vue.esm.js'
import lfs from '../res/lfs.js'
import parallaxEffect from './parallax.js'
import menu from './menu.js'
import dialog from './dialog.js'

Vue.component('ui-projects-screen', {
    template: `
        <div class='ui-projects-screen'>
            
            <span class='ui-list-header'>
                <ui-text text="Projects"></ui-text>
                <ui-button title="Create New project" class='active' @click.native='createProject'></ui-button>
            </span>
            
            <ui-list 
                :items="projects" 
                @ui-clicked="projectSelected"
                item-style="ui-list-item-project"
                class="ui-projects-list"
                ></ui-list>
        </div>
    `,
    data: _ => {
        return {
            projects: []
        }
    },
    props: {
    },
    methods: {
        updateProjects: function() {
            lfs.ready(async _ => {
                let items = await lfs.list('/')
                let archs = {}
                for (const c of Object.keys(items)) {
                    try {
                        const exists = await lfs.isFile(`/${c}/project.json`)
                        if (!exists) {
                            archs[c] = 'mima'
                        } else {
                            const data = JSON.parse(await lfs.get(`/${c}/project.json`))
                            archs[c] = data.architecture ?? 'mima'
                        }
                    } catch(_) {}
                }
                this.projects = Object.keys(items).map(c => {
                    return {
                        id: items[c],
                        title: c,
                        arch: archs[c]
                    }
                })
            })
        },
        projectSelected: function(project) {
            this.$el.dispatchEvent(new CustomEvent('open-project', {
                detail: project,
                bubbles: true
            }))
        },
        createProject: function() {
            this.$el.dispatchEvent(new CustomEvent('create-project', {
                detail: null,
                bubbles: true
            }))
        },
    },
    mounted: function() {
        this.updateProjects()
        lfs.watch(_ => { this.updateProjects() })
    }
})

Vue.component('ui-list-item-project', {
    template: `<span class="ui-list-item ui-list-item-project" :class="{disabled: isDisabled}" :style="[parallax]" @click="clicked">
        <ui-spot :x="pointerX" :y="pointerY" />
        <ui-text :text="item.title" class='title'></ui-text>
        <ui-text :text="item.arch"></ui-text>
        <ui-button class='round' icon='fas-ellipsis-v' @click.native="openMenu"></ui-button>
    </span>
    `,
    props: ['item'],
    mixins: [parallaxEffect],
    computed: {
        isDisabled() {
            return this.item.enabled == undefined
                ? false
                : !this.item.enabled
        }
    },
    methods: {
        clicked: function() {
            this.$emit("ui-click", this.item.id)
        },
        openMenu: function(evt) {
            evt.stopPropagation()
            menu.open(evt.target, [{items: [
                {
                    title: "Delete Project",
                    callback: _ => {
                        dialog.alert(evt.target, "Project", 
                            `Delete ${this.item.title}?\nYou cannot undo this action`, {
                                buttons: [{title: "Delete", action: "delete"}, {title: "Cancel", action: "cancel"}]
                        })
                        .then(r => {
                            if (r.action == 'delete') {

                                lfs.isDir(this.item.id).then(res => {
                                    if (res == true) {
                                        lfs.rmdir(this.item.id).catch(e => {
                                            dialog.alert(this.$el, "Project", e)
                                        })
                                    } else {
                                        lfs.rm(this.item.id).catch(e => {
                                            dialog.alert(this.$el, "Project", e)
                                        })
                                    }
                                }).catch(e => {
                                    dialog.alert(this.$el, "Project", e)
                                })
                            }
                        }, _ => {})
                    },
                }
            ]}])
        }
    }
})