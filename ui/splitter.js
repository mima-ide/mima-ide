import Vue from './vue.esm.js'

Vue.component('ui-hsplitter', {
    template: `
        <div class='ui-hsplitter'>
            <div class='ui-splitter-pane' :style='[leftWidth]'><slot name='left'></slot></div>
            <div class='ui-splitter-handle' ref='handle'>
                <div class='ui-splitter-knob'></div>
            </div>
            <div class='ui-splitter-pane'><slot name='right'></slot></div>
        </div>
    `,
    data: _ => {
        return {
            split: {
                default: 0.5
            }
        }
    },
    props: {
        preferredsplit: {
            default: 0.5
        }
    },
    computed: {
        leftWidth: function() {

            if (this.split == null) {
                return {}
            }
            return {
                width: `${this.split * 100}%`
            }
        }
    },
    mounted: function() {

        this.split = this.preferredsplit

        this.$refs.handle.addEventListener('mousedown', evt => {

            evt.preventDefault()
            evt.stopPropagation()

            let moveListener = evt => {

                evt.preventDefault()
                evt.stopPropagation()
                let left = evt.clientX - this.$el.getBoundingClientRect().left -3
                this.split = left / this.$el.offsetWidth

            }
            document.addEventListener('mousemove', moveListener, {capture: true})

            document.addEventListener('mouseup', evt => {

                document.removeEventListener('mousemove', moveListener, {capture: true})

            }, {once: true, capture: true})

        })

        document.addEventListener('resize', _ => {
            this.split = this.split
        })

    }
})

Vue.component('ui-vsplitter', {
    template: `
        <div class='ui-vsplitter'>
            <div class='ui-splitter-pane' :style='[topWidth]'><slot name='top'></slot></div>
            <div class='ui-splitter-handle' ref='handle'>
                <div class='ui-splitter-knob'></div>
            </div>
            <div class='ui-splitter-pane'><slot name='bottom'></slot></div>
        </div>
    `,
    data: _ => {
        return {
            split: {
                default: 0.5
            }
        }
    },
    props: {
        preferredsplit: {
            default: 0.5
        }
    },
    computed: {
        topWidth: function() {

            if (this.split == null) {
                return {}
            }

            return {
                height: `${this.split * 100}%`
            }
        }
    },
    mounted: function() {

        this.split = this.preferredsplit

        this.$refs.handle.addEventListener('mousedown', evt => {

            evt.preventDefault()
            evt.stopPropagation()

            let moveListener = evt => {

                evt.preventDefault()
                evt.stopPropagation()
                let top = evt.clientY - this.$el.getBoundingClientRect().top -3
                this.split = top / this.$el.offsetHeight

            }
            document.addEventListener('mousemove', moveListener, {capture: true})

            document.addEventListener('mouseup', evt => {

                document.removeEventListener('mousemove', moveListener, {capture: true})

            }, {once: true, capture: true})

        })

        document.addEventListener('resize', _ => {
            this.split = this.split
        })

    }
    
})