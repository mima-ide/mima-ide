import Vue from './vue.esm.js'
import lfs from '../res/lfs.js'
import dialog from './dialog.js'
import rfs from '../res/rfs.js'

Vue.component('ui-create-project-dialog', {
    template: `<ui-dialog class='ui-create-project-dialog'>
        <template v-slot:title>
            <ui-text :text="title"></ui-text>
            <input type='text' ref='filename' v-model="basename"></input>
        </template>
        <ui-dialog-view>
            <ui-project-template-picker ref="templatePicker" v-model="template"></ui-project-template-picker>
        </ui-dialog-view>
        <template v-slot:buttons>
            <ui-button title="Create" @click.native="create" class='active'></ui-button>
            <ui-button title="Cancel" @click.native="cancel"></ui-button>
            <div class='ui-div'></div>
        </template>
    </ui-dialog>
    `,
    data: _ => {
        return {
            basename: "",
            template: null
        }
    },
    props: {
        title: {
            default: "Project Name:"
        }
    },
    methods: {
        cancel: function() {
            this.clearTraps()
            this.$emit('cancel-dialog')
        },
        create: function() {

            if (!lfs.validName(this.basename)) {
                dialog.alert(this.$refs.templatePicker.$el, "Create Project", "Invalid name")
                return
            }

            let filename = lfs.normalize("/" + this.basename)

            this.clearTraps()
            this.$emit('accept-dialog', {action: 'create', value: {
                root: filename,
                template: this.template
            }})
        },
        clearTraps: function() {
            document.removeEventListener('keydown', this.keyTrap, {capture: true})
        },
        keyTrap: function(evt) {
            if (evt.key.toLowerCase() == "enter") {
                evt.preventDefault()
                evt.stopPropagation()
                this.create()
            } else if (evt.key.toLowerCase() == "escape") {
                evt.preventDefault()
                evt.stopPropagation()
                this.cancel()
            }
        }
    },
    mounted: function() {
        this.$refs.filename.focus()
        document.addEventListener('keydown', this.keyTrap, {capture: true})
        this.$el.addEventListener('dialog-open', evt => {
            this.clearTraps()
        })
        this.$el.addEventListener('dialog-close', evt => {
            document.addEventListener('keydown', this.keyTrap, {capture: true})
        })
    }

})


Vue.component('ui-project-template-picker', {
    template: `<div class='ui-project-template-picker'>
        <div class='ui-container'>
            <div class='ui-hstack' style='height: 100%'>
                <div style='position: relative; display: block; min-width: 200px; height: 100%'>
                    <div class='ui-scroll'>
                        <div class='ui-list-header'>
                            <ui-text text="Templates"></ui-text>
                        </div>
                        <ui-list :items="templates" :selected="[selectedTemplate]" @ui-clicked="templateSelected"></ui-list>
                    </div>
                </div>
                <div style='position: relative; height: 100%; flex-grow: 1'>
                    <div class='ui-scroll'>
                        <ui-markdown-view :url='readme'></ui-markdown-view>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `,
    data: _ => {
        return {
            templates: [],
            selectedTemplate: "templates/project/Empty_Project-mima"
        }
    },
    computed: {
        readme: function() {
            return this.selectedTemplate 
                ? this.selectedTemplate + "/readme.md"
                : null
        },
    },
    methods: {

        async loadTemplates() {

            const templateFiles = await rfs.list("templates/project")
            
            this.templates = Object.keys(templateFiles).map(name => {
                return {
                    title: this.titleFromFile(name),
                    id: templateFiles[name]
                }
            })

        },

        titleFromFile(name) {
            const parts = name.split("-")
            return parts[0].replace("_", " ") + ((parts.length >= 1) ? ` (${parts[1]})` : '')
        },

        templateSelected(name) {
            this.selectedTemplate = name
            this.$emit("input", this.selectedTemplate)
        }

    },
    mounted: function() {
        this.loadTemplates()
        this.$emit("input", this.selectedTemplate)
    }

})