import lfs from '../res/lfs.js'
import Vue from './vue.esm.js'

Vue.component('ui-dialog', {
    template: `
        <div class='ui-dialog'>
                <div class='ui-dialog-title'>
                    <slot name="title">
                        <ui-text :text="title"></ui-text>
                    </slot>
                </div>
                <div class='ui-dialog-content'>
                    <!--<div class='ui-scroll'>-->
                        <slot>
                            <ui-text :text="description"></ui-text>
                        </slot>
                    <!--</div>-->
                </div>
            <div class='ui-dialog-buttons'>
                <slot name='buttons'></slot>
            </div>
        </div>
    `,
    props: {
        title: {
            default: ""
        },
        icon: {
            default: null
        },
        description: {
            default: ""
        }
    },
    methods: {
        cancel: function() {
            this.clearTraps()
            this.$emit('cancel-dialog')
        },
        accept: function() {
            this.clearTraps()
            this.$emit('accept-dialog', {action: 'close', value: ""})
        },
        clearTraps: function() {
            document.removeEventListener('keydown', this.keyTrap, {capture: true})
        },
        keyTrap: function(evt) {
            if (evt.key.toLowerCase() == "enter") {
                evt.preventDefault()
                evt.stopPropagation()
                this.accept()
            } else if (evt.key.toLowerCase() == "escape") {
                evt.preventDefault()
                evt.stopPropagation()
                this.cancel()
            }
        }
    },
    mounted: function() {
        document.addEventListener('keydown', this.keyTrap, {capture: true})
    }
    
})

Vue.component('ui-dialog-view', {
    template: `
        <div class='ui-dialog-view' @dialog="openDialog">
            <slot></slot>
            <transition name='ui-dialog-fade'>
                <div class='ui-dialog-shrim' v-if='show' @click='closeDialog' >
                    <ui-dialog :is='dialogType' 
                        :title='title' 
                        :description='description' 
                        :buttons='buttons' 
                        :options='options'
                        @click.stop.native='' 
                        @cancel-dialog='closeDialog' 
                        @accept-dialog='acceptDialog'>
                        <template v-slot:buttons>
                            <ui-button v-for='button in buttons' 
                                :key='button.id'
                                :title='button.title' 
                                :icon='button.icon' 
                                :value='button.value' 
                                :action='button.action' 
                                :class='{active: isDefault(button)}'
                                @action='buttonClicked'></ui-button>
                        </template>
                    </ui-dialog>
                </div>
            </transition>
        </div>
    `,
    data: _ => {
        return {
            show: false,
            dialogType: 'ui-dialog',
            title: '',
            description: '',
            buttons: [],
            callback: _ => {}
        }
    },
    methods: {
        openDialog: function(evt) {
            evt.stopPropagation()
            evt.preventDefault()
            this.dialogType = evt.detail.type || 'ui-dialog'
            this.title = evt.detail.title
            this.description = evt.detail.description
            this.buttons = evt.detail.buttons || [{title: "Close", action: "close-dialog"}]
            this.callback = evt.detail.callback || (_ => {})
            this.options = evt.detail
            this.show = true

            this.$el.dispatchEvent(new CustomEvent('dialog-open', {bubbles: true}))

        },
        resetDialog: function() {
            this.show = false
            this.dialogType = 'ui-dialog'
            this.buttons = []
            this.title = []
            this.description = []
            this.buttons = []
            this.options = {}
            this.callback = _ => {}
            
            this.$el.dispatchEvent(new CustomEvent('dialog-close', {bubbles: true}))
        },
        closeDialog: function() {
            this.callback(false)
            this.resetDialog()
        },
        acceptDialog: function(data) {
            this.callback(data)
            this.resetDialog()
        },
        buttonClicked: function(evt) {

            if (evt.action == 'close-dialog' || evt.action == 'cancel') {
                this.closeDialog()
            } else {
                this.acceptDialog(evt)
            }

        },
        isDefault: function(button) {
            return button.action == 'ok' || button.action == 'yes' || button.action == 'close'
        }
    },
    mounted: function() {

        this.$el.addEventListener('dialog-open', evt => {
            if (evt.target != this.$el) {
                evt.stopPropagation()
            }
        })

        this.$el.addEventListener('dialog-close', evt => {
            if (evt.target != this.$el) {
                evt.stopPropagation()
            }
        })

    }

})

Vue.component('ui-dialog-open-file', {
    template: `
        <ui-dialog :title="title" class='ui-open-file-dialog'>
            <ui-dialog-view>
                <ui-file-picker v-model="location" :default-location="options.defaultLocation"></ui-file-picker>
            </ui-dialog-view>
            <template v-slot:buttons>
                <ui-button title="Open" @click.native="open" class='active'></ui-button>
                <ui-button title="Cancel" @click.native="cancel"></ui-button>
            </template>
        </ui-dialog>
    `,
    data: _ => {
        return {
            location: ["/"],
        }
    },
    props: {
        title: {
            default: "Open File"
        },
        options: {
            default: _ => {return {}}
        }
    },
    methods: {
        cancel: function() {
            this.clearTraps()
            this.$emit('cancel-dialog')
        },
        open: function() {
            this.clearTraps()
            this.$emit('accept-dialog', {action: 'open', value: this.location[0]})
        },
        clearTraps: function() {
            document.removeEventListener('keydown', this.keyTrap, {capture: true})
        },
        keyTrap: function(evt) {
            if (evt.key.toLowerCase() == "enter") {
                evt.preventDefault()
                evt.stopPropagation()
                this.open()
            } else if (evt.key.toLowerCase() == "escape") {
                evt.preventDefault()
                evt.stopPropagation()
                this.cancel()
            }
        }
    },
    mounted: function() {
        document.addEventListener('keydown', this.keyTrap, {capture: true})
        this.$el.addEventListener('dialog-open', evt => {
            this.clearTraps()
        })
        this.$el.addEventListener('dialog-close', evt => {
            document.addEventListener('keydown', this.keyTrap, {capture: true})
        })
    }
})

Vue.component('ui-dialog-save-file', {
    template: `
        <ui-dialog class='ui-save-file-dialog'>
            <template v-slot:title>
                <ui-text :text="title"></ui-text>
                <input type='text' ref='filename' v-model="basename"></input>
            </template>
            <ui-dialog-view>
                <ui-file-picker v-model="location" ref='filepicker' folders-only="true" :default-location="options.defaultLocation"></ui-file-picker>
            </ui-dialog-view>
            <template v-slot:buttons>
                <ui-button title="Save" @click.native="open" class='active'></ui-button>
                <ui-button title="Cancel" @click.native="cancel"></ui-button>
                <div class='ui-div'></div>
            </template>
        </ui-dialog>
    `,
    data: _ => {
        return {
            location: ["/"],
            basename: ""
        }
    },
    props: {
        title: {
            default: "Save File as"
        },
        options: {
            default: _ => {return {}}
        }
    },
    methods: {
        cancel: function() {
            this.clearTraps()
            this.$emit('cancel-dialog')
        },
        open: function() {

            if (!lfs.validName(this.basename)) {
                dialog.alert(this.$refs.filepicker.$el, "Save File", "Filename invalid")
                return
            }

            let filename = lfs.normalize(this.location[0] + "/" + this.basename)

            this.clearTraps()
            this.$emit('accept-dialog', {action: 'open', value: filename})
        },
        clearTraps: function() {
            document.removeEventListener('keydown', this.keyTrap, {capture: true})
        },
        keyTrap: function(evt) {
            if (evt.key.toLowerCase() == "enter") {
                evt.preventDefault()
                evt.stopPropagation()
                this.open()
            } else if (evt.key.toLowerCase() == "escape") {
                evt.preventDefault()
                evt.stopPropagation()
                this.cancel()
            }
        }
    },
    mounted: function() {

        this.$refs.filename.focus()
        document.addEventListener('keydown', this.keyTrap, {capture: true})
        this.$el.addEventListener('dialog-open', evt => {
            this.clearTraps()
        })
        this.$el.addEventListener('dialog-close', evt => {
            document.addEventListener('keydown', this.keyTrap, {capture: true})
        })
        if (this.options.defaultName != null) {
            this.basename = this.options.defaultName
        }
    }
})

Vue.component('ui-dialog-input', {
    template: `
        <ui-dialog :title="title" class='ui-dialog-input'>
            <ui-text :text='description'></ui-text>
            <input type='text' v-model='input' ref='input'></input>
            <template v-slot:buttons>
                <ui-button v-for='button in buttons' 
                    :key='button.id'
                    :title='button.title' 
                    :icon='button.icon' 
                    :value='button.value' 
                    :action='button.action' 
                    :class='{active: isDefault(button)}'
                    @action='buttonClicked'></ui-button>
            </template>
        </ui-dialog>
    `,
    props: {
        title: {
            default: null
        },
        description: {
            default: null
        },
        buttons: {
            default: _ => []
        }
    },
    data: _ => {
        return {
            input: ""
        }
    },
    methods: {
        cancel: function() {
            this.clearTraps()
            this.$emit('cancel-dialog')
        },
        accept: function() {
            this.clearTraps()
            this.$emit('accept-dialog', {action: 'ok', value: this.input})
        },
        buttonClicked: function(data) {
            if (data.action == 'close' || data.action == 'cancel') {
                this.cancel()
            } else if (data.action == 'ok' || data.action == 'yes') {
                this.accept()
            }
        },
        isDefault: function(button) {
            return button.action == 'ok' || button.action == 'yes' || button.action == 'close'
        },
        clearTraps: function() {
            document.removeEventListener('keydown', this.keyTrap, {capture: true})
        },
        keyTrap: function(evt) {
            if (evt.key.toLowerCase() == "enter") {
                evt.preventDefault()
                evt.stopPropagation()
                this.accept()
            } else if (evt.key.toLowerCase() == "escape") {
                evt.preventDefault()
                evt.stopPropagation()
                this.cancel()
            }
        }
    },
    mounted: function() {
        this.$refs.input.focus()
        document.addEventListener('keydown', this.keyTrap, {capture: true})
    }
})



let dialog = {
    open: function(el, options = {}) {

        return new Promise((resolve, reject) => {
            options.callback = r => {
                if (r == false) {
                    reject()
                } else {
                    resolve(r)
                }
            }
            el.dispatchEvent(new CustomEvent('dialog', {
                detail: options,
                bubbles: true
            }))
        })

    },
    alert: function(el, title, description, options = {}) {

        return new Promise((resolve, reject) => {
            options.callback = r => {
                if (r == false) {
                    reject()
                } else {
                    resolve(r)
                }
            }
            options.title = title
            options.description = description
            options.buttons = options.buttons || [{title: "Close", action: "close"}]
            el.dispatchEvent(new CustomEvent('dialog', {
                detail: options,
                bubbles: true
            }))
        })
    },
    input: function(el, title, description, options = {}) {
        return new Promise((resolve, reject) => {
            options.callback = r => {
                if (r == false) {
                    reject()
                } else {
                    resolve(r)
                }
            }
            options.type = 'ui-dialog-input'
            options.title = title
            options.description = description
            options.buttons = options.buttons || [{title: "OK", action: "ok"}, {title: "Cancel", action: "cancel"}]

            el.dispatchEvent(new CustomEvent('dialog', {
                detail: options,
                bubbles: true
            }))
        })
    },
    openFileDialog: function(el, options = {}) {
        return new Promise((resolve, reject) => {
            options.callback = r => {
                if (r == false) {
                    reject()
                } else {
                    resolve(r)
                }
            }
            options.type = 'ui-dialog-open-file'
            el.dispatchEvent(new CustomEvent('dialog', {
                detail: options,
                bubbles: true
            }))
        })
    },
    saveFileDialog: function(el, options = {}) {
        return new Promise((resolve, reject) => {
            options.callback = r => {
                if (r == false) {
                    reject()
                } else {
                    resolve(r)
                }
            }
            options.type = 'ui-dialog-save-file'
            el.dispatchEvent(new CustomEvent('dialog', {
                detail: options,
                bubbles: true
            }))
        })
    },
}

export default dialog