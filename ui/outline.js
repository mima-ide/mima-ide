import Vue from './vue.esm.js'
import parallaxEffect from './parallax.js'
import menu from './menu.js'

Vue.component('ui-outline', {
    template: `<span class="ui-outline">
        <transition-group name="ui-outline">
            <ui-outline-item v-for="i in items" :key="i.id" :item="i" :selected="selected" :menuFunction="menuFunction" @clicked="clicked"></ui-outline-item>
        </transition-group>
    </span>`,
    props: {
        items: {
            default: null
        },
        menuFunction: {
            default: async _ => {return id => []}
        },
        selected: {
            default: ""
        }
    },
    methods: {
        clicked(data) {
            this.$emit('clicked', data)
        }
    }
})

Vue.component('ui-outline-item', {
    data: _ => {
        return {
            expanded: true
        }
    },
    template: `<span class="ui-outline-item" :class="{active: isSelected}">
        <ui-outline-button :item="item" :expanded="expanded" v-on:click.native="clicked" :menuFunction="menuFunction"></ui-outline-button>
        <transition-group name='ui-outline' class="ui-outline-children" :class="{expanded: expanded}">
            <ui-outline-item v-for="i in item.items" :key="i.id" :item="i" :menuFunction="menuFunction" @clicked="childClicked" :selected="selected"></ui-outline-item>
        </transition-group>
    </span>`,
    props: {
        item: {
            default: null
        },
        menuFunction: {
            default: async _ => {return id => []}
        },
        selected: {
            default: ""
        }
    },
    computed: {
        isSelected() {
            return this.item.id == this.selected
        }
    },
    methods: {
        clicked: function() {
            this.toggleExpansion()
            this.$emit('clicked', this.item.id)
        },
        toggleExpansion: function() {
            this.expanded = !this.expanded
        },
        childClicked: function(data) {
            this.$emit('clicked', data)
        }
    }
})

Vue.component('ui-outline-button', {
    template: `<span class="ui-outline-button" :style="[parallax]" @contextmenu="openContextMenu">
        <ui-spot :x="pointerX" :y="pointerY" />
        <slot>
            <ui-icon v-if="item.items && item.items.length > 0 && !expanded" icon="fas-caret-right"></ui-icon>
            <ui-icon v-if="item.items && item.items.length > 0 && expanded" icon="fas-caret-down"></ui-icon>
            <ui-icon v-if="item.icon != null" :icon="item.icon"></ui-icon>
            <ui-text v-if="item.title != null" :text="item.title"></ui-text>
            <span class='ui-div'></span>
            <ui-icon icon="fas-ellipsis-h" class='menu' @click.native="openMenu"></ui-icon>
        </slot>
    </span>`,
    // props: ['item', 'title', 'icon', 'expanded'],
    props: {
        item: {default: null},
        expanded: {default: false},
        menuFunction: {default: async _ => {return id => []}}

    },
    mixins: [parallaxEffect],
    methods: {
        openMenu: function(evt) {
            evt.preventDefault()
            evt.stopPropagation()

            let currentTarget = evt.currentTarget

            this.menuFunction(this.item.id).then(menuItems => {
                menu.open(currentTarget, menuItems)
            })
            

        },
        openContextMenu: function(evt) {
            evt.preventDefault()
            evt.stopPropagation()

            let currentTarget = evt.currentTarget

            this.menuFunction(this.item.id).then(menuItems => {
                menu.contextmenu(currentTarget, evt, menuItems)
            })
        }
    }

})
