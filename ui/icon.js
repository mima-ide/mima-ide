import Vue from './vue.esm.js';

Vue.component('ui-icon', {
    template: `<span :class="[iconClass]"></span>`,
    props: ['icon'],
    computed: {
        iconClass: function() {
            let name = this.icon.substr(4);
            let type = this.icon.substr(0,3);
            return ['ui-icon', type, 'fa-' + name, 'fa-fw']
        }
    }
})