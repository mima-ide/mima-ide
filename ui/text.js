import Vue from './vue.esm.js';

Vue.component('ui-text', {
    template: '<span class="ui-text"><slot>{{text}}</slot></span>',
    props: ['text']
})