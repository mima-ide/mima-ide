import lfs from '../res/lfs.js'
import dialog from './dialog.js'
import Vue from './vue.esm.js'
import MimamicroHint from '../res/hints/mimamicro.js'
import CodeAnalysis from '../res/codeAnalysis/codeAnalysis.js'

Vue.component('ui-code-editor', {
    template: `
        <div class='ui-editor ui-code-editor'>
            <div ref='editor' class='editor' :class="{invisible: currentFile == ''}"></div>
        </div>
    `,
    data: _ => {
        return {
            documents: {},
            codeMirror: null,
            currentHints: {},
            linters: {},
            codeAnalysis: new CodeAnalysis()
        }
    },
    props: {
        currentFile: {
            default: ""
        },
        architecture: {
            default: "mima"
        },
    },
    watch: {
        currentFile() {

            if (this.currentFile != "") {
                this.showDocument(this.currentFile)
            }

        },
        architecture() {

            if (this.codeMirror.getOption("mode").match(/^mimasm/)) {
                this.codeMirror.setOption("mode", `mimasm-${this.architecture}`)
            }

        },
    },
    methods: {

        modeFromExtension(name) {
            let modes = {
                mima: `mimasm-${this.architecture}`,
                mimax: `mimasm-${this.architecture}`,
                mimasm: `mimasm-${this.architecture}`,
                mm: "mimamicro",
                micro: "mimamicro",
                json: "json",
                test: "testfixture"
            }
            return modes[lfs.extension(name)]
        },

        async showDocument(name) {

            if (this.documents[name] != null) {
                this.displayDocument(this.documents[name])
            } else {
                let doc = await this.loadDocument(name)
                this.displayDocument(doc)
            }

        },

        async loadDocument(name) {

            try {
                let contents = await lfs.get(name)

                const mode = this.modeFromExtension(name)
                let document = CodeMirror.Doc(contents, mode, 0)
                this.documents[name] = document
                this.loadHints(document, mode)
                this.applyLints(name)

                return document

            } catch(e) {
                dialog.alert(this.$el, "Editor", e)
            }
        },

        displayDocument(doc) {
            this.sleepHints(this.codeMirror.getDoc())
            this.codeMirror.swapDoc(doc)
            this.awakeHints(doc)
        },

        async saveDocument(name) {

            try {
                let doc = this.documents[name]
                if (doc != null) {
                    let contents = doc.getValue()
                    await lfs.put(name, contents)
                }
            } catch(e) {
                dialog.alert(this.$el, "Editor", e)
            }

        },

        async saveCurrentDocument() {

            await this.saveDocument(this.currentFile)
            this.lintFile()

        },

        async updateDocument(name) {

            let document = this.documents[name]
            let newContents = await lfs.get(name)

            if (document.getValue() != newContents) {
                document.setValue(newContents)
                this.applyLints(name)
            }

        },

        hintsForMode(mode) {

            if (mode == 'mimamicro') {
                return [new MimamicroHint()]
            }

            return []

        },

        loadHints(doc, mode) {

            const hints = this.hintsForMode(mode)
            hints.forEach(hint => hint.attach(doc))

            this.currentHints[doc.id] = hints 

        },

        sleepHints(doc) {
            const hints = this.currentHints[doc.id] ?? []
            hints.forEach(hint => hint.sleep(doc))
        },

        awakeHints(doc) {
            const hints = this.currentHints[doc.id] ?? []
            hints.forEach(hint => hint.awake(doc))
        },

        gotoLine(line) {

            this.codeMirror.scrollIntoView({line, ch: 0})
            this.codeMirror.setCursor({line, ch: 0})
            this.codeMirror.focus()

        },

        applyLints(doc) {

            const lints = this.linters[doc] ?? []

            const symbols = {
                info: "i",
                undefined: "?",
                error: '&#x21AF;',
                macro: 'M',
                subroutine: 'S',
                variable: 'V',
                "macro-invocation": "&#x21A2;",
                import: "&#x21A2;"
            }

            this.documents[doc].clearGutter("linter")
            this.documents[doc].clearGutter("analysis")

            for (let lint of lints) {
                let marker = document.createElement("span")
                marker.classList.add(`lint-${lint.type}`)

                marker.innerHTML = symbols[lint.type] ?? symbols.error

                marker.title = lint.text
                if (['import', 'macro', 'macro-invocation', 'subroutine', 'variable'].includes(lint.type)) {
                    this.documents[doc].setGutterMarker(lint.lineInfo.line, "analysis", marker)
                } else {
                    this.documents[doc].setGutterMarker(lint.lineInfo.line, "linter", marker)
                }
            }

        },

        async lintFile() {

            const lints = await this.codeAnalysis.lintFile(this.architecture, this.currentFile)
            console.log('lints', lints)

            const lintedFiles = lints.map(l => l.lineInfo.file).filter((x, i, a) => a.indexOf(x) == i)
            for (const file of lintedFiles) {
                this.linters[file] = lints.filter(l => l.lineInfo.file == file)
            }

            this.applyLints(this.currentFile)

        }

    },
    mounted: function() {
        this.codeMirror = CodeMirror(this.$refs.editor, {
            lineNumbers: true,
            theme: "shadowfox",
            mode: "mimasm",
            firstLineNumber: 0,
            gutters: ['CodeMirror-linenumbers', 'analysis', 'linter'],
            value: "",
            indentUnit: 4,
            tabSize: 4,
            indentWithTabs: false,
            smartIndent: true,
            extraKeys: {
                Tab: cm => {
                    if (cm.somethingSelected()) {
                        cm.indentSelection("add");
                    } else {
                        cm.replaceSelection(
                            cm.getOption("indentWithTabs")
                            ? "\t"
                            : Array(cm.getOption("indentUnit") + 1).join(" "), "end", "+input");
                    }
                }
            }
        })

        lfs.watch((method, name) => {

            if (method != "put") { return }
            if (!Object.keys(this.documents).includes(name)) { return }

            this.updateDocument(name)

        })

        this.codeMirror.on("mousedown", (cm, evt) => {
            if (evt.ctrlKey || evt.metaKey) {
                evt.preventDefault()
                const pos = cm.cursorCoords(true, "page")
                console.log("POS", pos)

                let elem = document.createElement("div")
                elem.style.width = '20px'
                elem.style.height = '20px'
                elem.style.background = "red"
                elem.style.top = `${pos.top}px`
                elem.style.left = `${pos.left}px`
                elem.style.position = 'absolute'

                document.body.appendChild(elem)

            }
        });

    
    }
})