import MemoryMap from '../res/memoryMap.js'
import Vue from './vue.esm.js'
import Menu from './menu.js'
import Opcodes from '../res/assembler/opcodes.js'
import findMenu from './findMenu.js'
import menu from './menu.js'
import dialog from './dialog.js'

Vue.component('ui-memory-editor', {
    template: `
        <div class='ui-editor ui-memory-editor'>
            <div ref='editor' class='editor'></div>
            <div class='mini-toolbar'>
                <ui-button
                    icon="fas-search"
                    tip="Find Symbol"
                    @click.native="openSymbolMenu"
                    ></ui-button>
                <ui-button
                    icon='fas-sync-alt'
                    tip='Follow Instruction Pointer'
                    :class='{active: followIP}'
                    @click.native="followIP = !followIP"
                    ></ui-button>
                <span class='ui-div'></span>
                <ui-button 
                    class="section-selector" 
                    icon="fas-sort"
                    :title="selectedSectionTitle"
                    @click.native="openSectionMenu"></ui-button>
                
            </div>
    </div>
    `,
    data: _ => {
        return {
            cm: null,
            selectedSection: ".all",
            lineMapping: [],
            sectionMapping: [],
            followIP: false,
            showSymbolMenu: false
        }
    },
    props: {
        memory: {
            default: ""
        },
        sections: {
            default: _ => { return [] }
        },
        symbols: {
            default: _ => { return [] }
        },
        defaultSection: {
            default: ".all"
        },
        instructionPointer: {
            default: 0
        },
        architecture: {
            default: "mima"
        },
        breakpoints: {
            default: _ => { return [] }
        },
        shouldFollowIP: {
            default: false
        },
    },
    watch: {
        memory() {
            this.cm.refresh()
            this.updateMemory()
        },

        sections() {
            this.recalculateLineMapping()
            this.updateInstructionPointer()
            this.updateBreakpoints()

            this.cm.setOption("mode", `memory-${this.architecture}`)
        },

        selectedSection() {
            this.recalculateLineMapping()
            this.updateInstructionPointer()
            this.updateBreakpoints()
        },

        instructionPointer() {
            this.updateInstructionPointer()
        },

        breakpoints() {
            this.updateBreakpoints()
        },

        architecture() {

            this.cm.setOption("mode", `memory-${this.architecture}`)

        },
    },
    computed: {
        sectionMenu() {

            return this.sections.map(section => {

                const symbol = this.symbolForAddress(section.location) || "$" + section.location.toString(16)
                return {
                    id: section.location,
                    title: `${symbol} (.${section.type})`,
                    callback: _ => {
                        this.selectedSection = section.location
                    }
                }

            })

        },
        fullSectionMenu() {

            return [
                {title: "", items: [
                    {id: ".all", title: "All Sections", callback: _ => { this.selectedSection = ".all" }},
                    {id: ".data", title: ".data Sections", callback: _ => { this.selectedSection = ".data" }},
                    {id: ".text", title: ".text Sections", callback: _ => { this.selectedSection = ".text" }}
                ]},
                {title: "", items: this.sectionMenu}
            ]

        },

        selectedSectionTitle() {

            if ([".data", ".text"].includes(this.selectedSection)) {
                return `${this.selectedSection} Sections`
            } else if(this.selectedSection == ".all") {
                return "All Sections"
            } else {
                const symbol = this.symbolForAddress(this.selectedSection) || "$" + this.selectedSection.toString(16)
                const type = this.sectionType(this.selectedSection)
                return `${symbol} (.${type})`
            }

        },

        activeSections() {

            if (this.selectedSection == ".all") {
                return this.sections.map(s => s)
            } else if (this.selectedSection == ".data") {
                return this.sections.filter(sec => {return sec.type == "data"})
            } else if (this.selectedSection == ".text") {
                return this.sections.filter(sec => {return sec.type == "text"})
            } else {
                return this.sections.filter(sec => {return sec.location == this.selectedSection})
            }

        },

        memoryMap() {

            try {
                return MemoryMap.fromText(this.memory)
            }catch(e) {
                return {}
            }

        },

        symbolsReverse() {

            let reverse = {}
            
            this.symbols.forEach(symb => {
                reverse[symb.address] = symb.name
            })

            return reverse

        },

        symbolMenu() {

            return this.symbols.map(symb => {

                return {
                    id: symb.name,
                    title: symb.name,
                    callback: _ => {
                        this.showSymbolMenu = false
                        this.goToSymbol(symb.name)
                    }
                }

            })

        },

    },
    methods: {

        symbolForAddress(address) {

            return (this.symbols.filter(symb => {return symb.address == address})[0] ?? {name: ""})
                    .name

        },

        addressForSymbol(symbol) {

            return (this.symbols.filter(symb => {return symb.name == symbol})[0] ?? {address: null})
                    .address

        },

        openSectionMenu(evt) {

            Menu.open(evt.target, this.fullSectionMenu)

        },

        sectionType(address) {
            return (this.sections.filter(sec => {return sec.location == address})[0] ?? {type: ""})
                .type
        },

        recalculateLineMapping() {

            const sections = this.activeSections.sort((a, b) => a.location - b.location)
            const lines = []
            const sectionMapping = []

            sections.forEach(section => {

                sectionMapping.push({line: lines.length, section})
                for (let i = 0; i < section.size; ++i) {
                    lines.push(section.location + i)
                }

            })

            this.lineMapping = lines

            let blindText = []
            for (let i = 0; i < this.lineMapping.length; ++i) {
                blindText.push("")
            }

            this.cm.setValue(blindText.join("\n"))

            sectionMapping.forEach(s => {
                this.addSectionMarker(s.line, this.getSectionTitle(s.section))
            })

            this.sectionMapping = sectionMapping

            this.updateMemory()

        },

        getSectionTitle(section) {

            const symbol = this.symbolForAddress(section.location) || "$" + section.location.toString(16)
            return `${symbol} (.${section.type})`

        },

        updateMemory() {

            for (let i = 0; i < this.lineMapping.length; ++i) {

                const address = this.lineMapping[i]
                let value = this.memoryMap[address] ?? 0
                const hex = ("000000" + parseInt(value).toString(16)).substr(-6)
                const section = this.sectionForLine(i)

                const printedValue = this.printedValue(value)
                let data = `0x${hex} ${printedValue}`
                if (section.type == "text") {
                    const mnemonic = this.getMnemonic(value)
                    data = `0x${hex} ${mnemonic}`
                }

                

                
                try {
                    this.cm.getDoc().replaceRange(data, {line: i, ch: 0}, {line: i})
                    this.addAddress(i, address)

                    const symbol = this.symbolForAddress(address)
                    if (symbol != "") { this.addSymbol(i, symbol) }

                } catch(_) {}

            }

            this.updateInstructionPointer()

        },

        printedValue(value) {
            if (value < 0x800000) {
                return value
            } else {
                return value | (-1 ^ 0xffffff)
            }
        },

        addSymbol(line, name) {

            const marker = document.createElement("span")
			marker.innerHTML = name
            marker.classList.add("symbol")
            marker.title = name
			this.cm.getDoc().setGutterMarker(line, "symbols", marker)


        },

        addAddress(line, address) {

            const marker = document.createElement("span")
			marker.innerHTML = "0x" + ("000000" + parseInt(address).toString(16)).substr(-6)
			marker.classList.add("address")
			this.cm.getDoc().setGutterMarker(line, "addresses", marker)

        },

        addSectionMarker(line, sectionTitle) {

            const el = document.createElement("div")
			el.classList.add("section-start")
			el.innerText = `${sectionTitle}`

			const widget = this.cm.getDoc().addLineWidget(line, el, {
				above: true
			})


        },

        addBreakpointMarker(line) {

            let marker = document.createElement("span")
			marker.classList.add("breakpoint")
			marker.innerHTML = "&bull;"

			this.cm.getDoc().setGutterMarker(line, "breakpoints", marker)

        },

        updateInstructionPointer() {

            this.cm.getDoc().clearGutter("debug-infos")
            this.updateStackPointer()

            let line = this.lineForAddress(this.instructionPointer)
            if (line == null) { return }

            

            let marker = document.createElement("span")
            marker.classList.add("debug-info")
            marker.innerHTML = "&#x279c;";
            marker.title = "Instruction Pointer"

            this.cm.getDoc().setGutterMarker(line, "debug-infos", marker)

            if (this.followIP) {
                this.goToAddress(this.instructionPointer, false)
            }

        },

        updateStackPointer() {

            const spAddress = this.addressForSymbol("SP")
            const bpAddress = this.addressForSymbol("BP") ?? this.addressForSymbol("FP")

            const topOfStackPointer = this.memoryMap[this.memoryMap[spAddress] ?? []]
            if (topOfStackPointer != null) {
                const topOfStackPointerLine = this.lineForAddress(topOfStackPointer)
                if (topOfStackPointerLine != null) {
                    const tospMarker = document.createElement("span")
                    tospMarker.classList.add("return-pointer")
                    tospMarker.innerHTML = "&#x21DD;";
                    tospMarker.title = "Return Pointer"
                    this.cm.getDoc().setGutterMarker(topOfStackPointerLine, "debug-infos", tospMarker)
                }
            } 

            if (bpAddress != null) {
                const bpLine = this.lineForAddress(this.memoryMap[bpAddress])
                if (bpLine != null) {
                    const bpMarker = document.createElement("span")
                    bpMarker.classList.add("base-pointer")
                    bpMarker.innerHTML = "&#x21B3;";
                    bpMarker.title = "Base Pointer"
                    this.cm.getDoc().setGutterMarker(bpLine, "debug-infos", bpMarker)
                }
            }

            if (spAddress != null) {

                

                const spLine = this.lineForAddress(this.memoryMap[spAddress])
                if (spLine != null) {
                    const spMarker = document.createElement("span")
                    spMarker.classList.add("stack-pointer")
                    spMarker.innerHTML = (this.memoryMap[spAddress] == this.memoryMap[bpAddress]) ? "&#x21C9;" : "&#x21B1;"
                    spMarker.title = "Stack Pointer"
                    this.cm.getDoc().setGutterMarker(spLine, "debug-infos", spMarker)
                }

                

            }

            


        },

        lineForAddress(address) {
            for (let i = 0; i < this.lineMapping.length; ++i) {
                if (this.lineMapping[i] == address) {
                    return i
                }
            }

            return null
        },

        addressForLine(line) {

            return this.lineMapping[line]

        },

        getMnemonic(value) {

            return Opcodes.getMnemonic(value, this.symbolsReverse, this.architecture)

        },

        sectionForLine(line) {

            for (let sec of this.sectionMapping.sort((a, b) => b.line - a.line)) {
                if (line >= sec.line) {
                    return sec.section
                }
            }

            return {type: ""}

        },

        updateBreakpoints() {

            this.cm.getDoc().clearGutter("breakpoints")

            this.breakpoints.forEach(address => {
                const line = this.lineForAddress(address)
                if (line == null) { return }
                
                this.addBreakpointMarker(line)
            })

        },

        toggleBreakpoint(address) {

            this.$el.dispatchEvent(new CustomEvent('toggle-breakpoint', {
                detail: address,
                bubbles: true
            }))


        },

        setIP(address) {

            this.$el.dispatchEvent(new CustomEvent('set-instruction-pointer', {
                detail: address,
                bubbles: true
            }))


        },

        goToAddress(address, switchView = false) {

            let line = this.lineForAddress(address)
            if (line == null) { 
                if (switchView) {
                    this.selectedSection = ".all"
                    this.goToAddress(address, false)
                }
                return
            }

            this.cm.scrollIntoView({line, ch: 0})

        },

        goToSymbol(symbol) {

            let address = this.addressForSymbol(symbol)
            if (address == null) { return }

            this.goToAddress(address, true)

        },

        openSymbolMenu(evt) {

            findMenu.open(evt.target, this.symbolMenu)

        },

        setWatcher(address) {

            let symbol = this.symbolForAddress(address)
            if (symbol == "") {
                symbol = '$' + address.toString(16)
            }

            this.$el.dispatchEvent(new CustomEvent('add-watcher', {
                detail: {
                    name: symbol,
                    address
                },
                bubbles: true
            }))

        },

        editValue(address) {

            dialog.open(this.$el, {
                type: 'ui-memory-edit-dialog',
                value: this.memoryMap[address],
                architecture: this.architecture,
                name: `0x${address.toString(16)}`,
                symbols: this.symbols,
            }).then(r => {
                if (r.action == 'save') {
                    this.$el.dispatchEvent(new CustomEvent('set-address', {
                        detail: {
                            address,
                            value: r.value
                        },
                        bubbles: true
                    }))
                }
            }, _ => {})

        },

    },
    mounted: function() {
        this.cm = CodeMirror(this.$refs.editor, {
            lineNumbers: true,
            theme: "shadowfox",
            mode: `memory-${this.architecture}`,
            readOnly: true,
            firstLineNumber: 0,
            lineNumbers: false,
            gutters: ['symbols', 'addresses', 'breakpoints', 'debug-infos', ],
            value: ""
        })

        this.cm.on("mousedown", (cm, evt) => {
            if (evt.ctrlKey || evt.metaKey) {
                evt.preventDefault()
                const pos = cm.coordsChar({left: evt.clientX, top: evt.clientY})
                const token = cm.getTokenAt(pos)

                this.goToSymbol(token.string)

            }
        });

        CodeMirror.on(this.cm.getWrapperElement(), "mouseover", (evt, cm) => {

            if (!evt.ctrlKey && !evt.metaKey) { return }

            this.cm.getWrapperElement().classList.add('cm-hover')

            const pos = this.cm.coordsChar({left: evt.clientX, top: evt.clientY});
            const token = this.cm.getTokenAt(pos);
            if (token.type != "variable") { return }
        });

        CodeMirror.on(this.cm.getWrapperElement(), "mouseout", (evt, cm) => {

            this.cm.getWrapperElement().classList.remove('cm-hover')

        });

        this.cm.on("gutterClick", (_, line, gutter) => {
            if (gutter != "breakpoints") { return }

            this.toggleBreakpoint(this.addressForLine(line))
        })

        this.cm.on("gutterContextMenu", (_, line, gutter, evt) => {

            evt.preventDefault()
            const address = this.addressForLine(line)

            menu.contextmenu(evt.target, evt, [{items: [
                {title: "Toggle Breakpoint", callback: _ => {
                    this.toggleBreakpoint(address)
                }},
                {title: "Move IP here", callback: _ => {
                    this.setIP(address)
                }},
                {title: "Watch", callback: _ => {
                    this.setWatcher(address)
                }},
                {title: "Edit Value", callback: _ => {
                    this.editValue(address)
                }},
                {title: "Go to definition", callback: _ => {
                    this.$el.dispatchEvent(new CustomEvent('goto-line-of-address', {
                        detail: address,
                        bubbles: true
                    }))
                }}
            ]}])
            
        })

        this.selectedSection = this.defaultSection
        this.followIP = this.shouldFollowIP
    
    }
})