import MemoryMap from '../res/memoryMap.js'
import dialog from './dialog.js'
import findMenu from './findMenu.js'
import menu from './menu.js'
import Vue from './vue.esm.js'

Vue.component('ui-register-editor', {
    template: `
        <div class='ui-register-editor'>
            
            <ui-register v-for="r in renderedRegisters" 
                :key="r.id" 
                :title="r.title" 
                :value="r.value" 
                :watcher="r.watcher" 
                :architecture="architecture" 
                :symbols="symbols"></ui-register>
            <div class='ui-div'></div>
            <ui-button
                icon='fas-plus'
                tip="Add Watchpoint"
                @click.native="newWatcher"
                ></ui-button>

        </div>
    `,
    data: _ => {
        return {
        }
    },
    props: {
        registers: {
            default: _ => { 
                return {
                    akku: 0,
                }
            }
        },
        memory: {
            default: ""
        },
        watcher: {
            default: _ => {}
        },
        symbols: {
            default: _ => []
        },
        architecture: {default: 'mima'},
    },
    computed: {
        memoryMap() {
            try {
                return MemoryMap.fromText(this.memory)
            } catch(_) {
                return {}
            }
        },
        renderedRegisters() {
            let list = []
            list.push({id: "akku", title: "AKKU", value: this.registers.AKKU ?? 0, watcher: null})
            list.push({id: "akkup", title: "*AKKU", value: this.memoryAt(this.registers.AKKU ?? 0), watcher: null})
            for (let reg of Object.keys(this.registers).filter(r => r != "AKKU")) {
                list.push({
                    id: reg,
                    title: reg,
                    value: this.registers[reg],
                    watcher: null,
                })
            }

            list = list.concat(this.watcher.map(w => {
                return {
                    id: w.name,
                    title: w.name,
                    value: this.memoryAt(w.address),
                    watcher: w.address,
                }
            }))

            return list
        },
        symbolsMenu() {

            return this.symbols.map(s => {
                return {
                    id: s.name,
                    title: s.name,
                    callback: _ => {
                        this.addWatcher(s.name, s.address)
                    }
                }
            })

        },
    },
    methods: {
        memoryAt(address) {
            return this.memoryMap[address]
        },
        addWatcher(name, address) {
            this.$el.dispatchEvent(new CustomEvent('add-watcher', {
                detail: {
                    name,
                    address
                },
                bubbles: true
            }))
        },
        newWatcher(evt) {
            findMenu.open(evt.target, this.symbolsMenu)
        },
    }
})

Vue.component('ui-register', {
    template: `
        <div class='ui-register' @contextmenu='contextmenu' @dblclick='openEditor'>
            
            <strong>{{title}}</strong>
            <div class='ui-vstack'>
                <p>{{hexvalue}}</p>
                <p>{{printedValue}}</p>
            </div>

        </div>
    `,
    props: {
        title: {default: ""},
        value: {default: 0},
        watcher: {default: null},
        architecture: {default: 'mima'},
        symbols: {default: _ => {return []}},
    },
    computed: {
        hexvalue: function() {
            const num = ("000000" + this.value.toString(16)).slice(-6)
            return `0x${num}`
        },
        printedValue: function() {
            if (this.value < 0x800000) {
                return this.value
            } else {
                return this.value | (-1 ^ 0xffffff)
            }
        },
    },
    methods: {
        contextmenu(evt) {
            evt.preventDefault()
            evt.stopPropagation()

            if (this.watcher != null) {

                menu.contextmenu(this.$el, evt, [{items: [
                    {title: "Edit Value", callback: _ => {

                        this.openEditor(evt)

                    }},
                    {title: "Hide Watcher", callback: _ => {

                        this.$el.dispatchEvent(new CustomEvent('remove-watcher', {
                            detail: {
                                address: this.watcher
                            },
                            bubbles: true
                        }))

                    }},
                ]}])

            } else {

                menu.contextmenu(this.$el, evt, [{items: [
                    {title: "Edit Value", callback: _ => {

                        this.openEditor(evt)

                    }},
                ]}])

            }

        },
        openEditor(evt) {

            dialog.open(this.$el, {
                type: 'ui-memory-edit-dialog',
                value: this.value,
                architecture: this.architecture,
                name: this.title,
                symbols: this.symbols,
            }).then(r => {
                if (r.action == 'save') {
                    if (this.watcher == null) {
                        this.setRegister(this.title, r.value)
                    } else {
                        this.setMemoryLocation(this.watcher, r.value)
                    }
                }
            }, _ => {})



        },
        setRegister(register, value) {
            this.$el.dispatchEvent(new CustomEvent('set-register', {
                detail: {
                    register,
                    value
                },
                bubbles: true
            }))
        },
        setMemoryLocation(address, value) {
            this.$el.dispatchEvent(new CustomEvent('set-address', {
                detail: {
                    address,
                    value
                },
                bubbles: true
            }))
        },
    }
})