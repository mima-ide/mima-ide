import Vue from './vue.esm.js'
import parallaxEffect from './parallax.js'

Vue.component('ui-notebook-tab', {
    template: `<span class="ui-notebook-tab" :style="[parallax]">
        <ui-spot :x="pointerX" :y="pointerY" />
        <slot>
            <ui-icon v-if="icon != null" :icon="icon"></ui-icon>
            <ui-text v-if="title != null" :text="title"></ui-text>
        </slot>
    </span>`,
    props: ['title', 'icon'],
    mixins: [parallaxEffect]

})

Vue.component('ui-notebook-tabs', {
    template: `<span class="ui-notebook-tabs">
        <slot></slot>
    </span>`,
    props: []
})
