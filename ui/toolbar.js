import Vue from './vue.esm.js'

Vue.component('ui-toolbar', {
    template: `
        <div class='ui-toolbar'>
            <div class='ui-toolbar-title'>
                <ui-text :text="title"></ui-text>
                <slot name='herobutton'></slot>
            </div>
            <div class='ui-toolbar-content'>
                <div class='ui-toolbar-left'><slot name='left'></slot></div>
                <div class='ui-toolbar-status'>
                    <div class='ui-toolbar-status-determinate-once' v-if="status == 'finished'">
                        <div class='ui-toolbar-status-determinate-once-spot'></div>
                    </div>
                    <div class='ui-toolbar-status-indeterminate' v-if="status == 'working'">
                        <div class='ui-toolbar-status-indeterminate-spot'></div>
                    </div>
                    <slot name='status'></slot>
                </div>
                <div class='ui-toolbar-right'><slot name='right'></slot></div>
            </div>
        </div>
    `,
    props: {

        title: {
            type: String,
            default: ""
        },

        status: {
            type: String,
            default: ""
        }
    },
    methods: {}
})