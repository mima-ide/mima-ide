import Vue from './vue.esm.js'
import MemoryMap from '../res/memoryMap.js'
import Microcode from '../res/microcode.js'

Vue.component('ui-microcode-memory-editor', {
    template: `
        <div class='ui-editor ui-memory-editor'>
            <div ref='editor' class='editor'></div>
        </div>
    `,
    data: _ => {
        return {
            cm: null
        }
    },
    props: {
        memory: {
            default: ""
        },
        instructionPointer: {
            default: 0
        },
        instructionRegister: {
            default: 0
        }
    },
    computed: {
        
    },
    watch: {
        memory() {
            this.updateMemory()
        },
        instructionPointer() {
            this.updateIP()
        }
    },
    methods: {
        updateMemory() {
            if (this.memory == null) return
            const mem = MemoryMap.fromText(this.memory)
            const lastAddress = Math.max.apply(null, Object.keys(mem))

            let lines = []
            for (let i = 0; i < lastAddress + 1; ++i) {
                const code = mem[i] ?? 0
                const description = Microcode.signalDescription(Microcode.signalsFromMicrocode(code))
                lines[i] = `0x${this.formatHex(code)} ${description}`
            }

            this.cm.setValue(lines.join("\n"))

            this.updateIP()

        },
        formatHex(number) {
            return ("0000000" + number.toString(16)).substr(-7)
        },
        updateIP() {

            if (this.memory == null) return

            const line = this.instructionPointer

            this.cm.getDoc().clearGutter("debug-infos")

            let marker = document.createElement("span")
            marker.classList.add("debug-info")
            marker.innerHTML = "&#x279c;";

            this.cm.getDoc().setGutterMarker(line, "debug-infos", marker)

            try {
                this.cm.scrollIntoView({line, ch: 0})
            } catch(_) {}

            const mem = MemoryMap.fromText(this.memory)
            const currentSignals = Microcode.signalsFromMicrocode(mem[this.instructionPointer])
            let nextLine = mem[this.instructionPointer] & 0xff
            if (currentSignals.Ir) {
                const primaryOpcode = this.instructionRegister >> 20
                const secondaryOpcode = primaryOpcode == 0xf ? (this.instructionRegister >> 16) & 0xf : 0
                nextLine = line + primaryOpcode + secondaryOpcode + 1
            }

            let nextMarker = document.createElement("span")
            nextMarker.classList.add("return-pointer")
            nextMarker.innerHTML = "&#x21DD;";

            this.cm.getDoc().setGutterMarker(nextLine, "debug-infos", nextMarker)

        }
    },
    mounted() {
        this.cm = CodeMirror(this.$refs.editor, {
            lineNumbers: true,
            theme: "shadowfox",
            mode: "mimamicromemory",
            readOnly: true,
            firstLineNumber: 0,
            lineNumbers: true,
            gutters: ['CodeMirror-linenumbers', 'debug-infos', ],
            value: "",
            lineNumberFormatter: n => {
                const num = ("00" + n.toString(16)).slice(-2)
                return `0x${num}`
            },
        })

        this.updateMemory()
        this.updateIP()

    }
})