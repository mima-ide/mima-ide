import Vue from './vue.esm.js'
import parallaxEffect from './parallax.js'

Vue.component('ui-list-item', {
    template: `
    <span class="ui-list-item" :class="{disabled: isDisabled}" :style="[parallax]" @click="clicked">
        <ui-spot :x="pointerX" :y="pointerY" />
        <strong v-if="item.title != null">{{item.title}}</strong>
        <p v-if="item.text != null">{{item.text}}</p>
    </span>`,
    props: {
        item: Object
    },
    mixins: [parallaxEffect],
    computed: {
        isDisabled() {
            return this.item.enabled == undefined
                ? false
                : !this.item.enabled
        }
    },
    methods: {
        clicked: function() {
            this.$emit("ui-click", this.item.id)
        },
        
    }
    
})

Vue.component('ui-list-item-simple', {
    template: `<span class="ui-list-item ui-list-item-simple" :class="{disabled: isDisabled}" :style="[parallax]" @click="clicked">
        <ui-spot :x="pointerX" :y="pointerY" />
        <ui-icon :icon="item.icon" v-if="item.icon != null"></ui-icon>
        <ui-text :text="item.title"></ui-text>
    </span>
    `,
    props: ['item'],
    mixins: [parallaxEffect],
    computed: {
        isDisabled() {
            return this.item.enabled == undefined
                ? false
                : !this.item.enabled
        }
    },
    methods: {
        clicked: function() {
            this.$emit("ui-click", this.item.id)
        }
    }
})

Vue.component('ui-list-item-image', {
    template: `<span class="ui-list-item ui-list-item-image" :style="[parallax]" @click="clicked">
        <ui-spot :x="pointerX" :y="pointerY" />
        <ui-image :src="item.image" image-style="rounded" />
        <span class="ui-vstack">
            <strong v-if="item.title != null">{{item.title}}</strong>
            <p v-if="item.text != null">{{item.text}}</p>
        </span>
    </span>
    `,
    props: ['item'],
    mixins: [parallaxEffect],
    methods: {
        clicked: function() {
            this.$emit("ui-click", this.item.id)
        }
    }
})

Vue.component('ui-list-item-contact', {
    template: `<span class="ui-list-item ui-list-item-contact" :style="[parallax]" @click="clicked">
        <ui-spot :x="pointerX" :y="pointerY" />
        <ui-image :src="item.image" image-style="contact" />
        <span class="ui-vstack">
            <strong v-if="item.title != null">{{item.title}}</strong>
            <p v-if="item.text != null">{{item.text}}</p>
        </span>
    </span>
    `,
    props: ['item'],
    mixins: [parallaxEffect],
    methods: {
        clicked: function() {
            this.$emit("ui-click", this.item.id)
        }
    }
})


Vue.component('ui-list', {
    template: `
    <transition-group class="ui-list" name="list">
        <ui-list-item :is="itemStyle" :class="{selected: isSelected(item)}" v-for="item in items" :key="item.id" :item="item" @ui-click="itemClicked" />
    </transition-group>`,
    props: {
        items: Array,
        itemStyle: {
            type: String,
            default: "ui-list-item"
        },
        selected: {
            type: Array,
            default: _ => {return []}
        }
    },
    methods: {
        itemClicked: function(id) {

            this.$emit('ui-clicked', id)
        },
        isSelected: function(item) {
            return this.selected.includes(item.id)
        }
    }
})