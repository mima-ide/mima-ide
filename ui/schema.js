import Vue from './vue.esm.js'

Vue.component('ui-schema', {
    template: `
        <div class='ui-schema'>
            <svg viewBox='0 0 100 110' xmlns="http://www.w3.org/2000/svg" style='height: 100%'>
        
            <line class='connection' x1='30' y1='7' x2='22' y2='7'
                :class="{write: controlSignals.Aw, read: controlSignals.Ar}" />
        
            <rect class='register' x='2' y='2' width='20' height='10' 
                :class="{write: controlSignals.Aw, read: controlSignals.Ar}" />
            <text class='register' x='3' y='9'>AKKU</text>
        
        
            <line class='connection' x1='30' y1='22' x2='22' y2='22'
                :class="{write: controlSignals.E}" />
        
            <rect class='register' x='2' y='17' width='20' height='10' 
                :class="{write: controlSignals.E}" />
            <text class='register' x='3' y='24'>EINS</text>
        
        
            <line class='connection' x1='30' y1='7' x2='47' y2='7' 
                :class="{write: controlSignals.Pw, read: controlSignals.Pr}" />
        
            <rect class='register' x='47' y='2' width='20' height='10' 
                :class="{write: controlSignals.Pw, read: controlSignals.Pr}" />
            <text class='register' x='48' y='9'>IAR</text>
        
        
            <line class='connection' x1='30' y1='22' x2='47' y2='22' 
                :class="{write: controlSignals.Iw, read: controlSignals.Ir}"/>
        
            <rect class='register' x='47' y='17' width='20' height='10' 
                :class="{write: controlSignals.Iw, read: controlSignals.Ir}" />
            <text class='register' x='48' y='24'>IR</text>
        
        
            <rect class='unit' x='75' y='17' width='20' height='20' />
            <text class='unit' x='76' y='27'>
                <tspan x='76' dy='1em'>Steuer-</tspan>
                <tspan x='76' dy='1em'>werk</tspan>
            </text>
        
        
            <line class='connection' x1='30' y1='37' x2='47' y2='37' 
                :class="{write: controlSignals.Z}" />
        
            <rect class='register' x='47' y='32' width='20' height='10' 
                :class="{write: controlSignals.Z}" />
            <text class='register' x='48' y='39'>Z</text>
        
            <line class='connection' x1='57' y1='42' x2='57' y2='47' 
                :class="{read: controlSignals.C0 || controlSignals.C1 || controlSignals.C2}" />
        
            <polygon class='unit' points='47,47 67,47 76,63 62,63 57,57 52,63 37,63' 
                :class="{use: controlSignals.C0 || controlSignals.C1 || controlSignals.C2}" />
            <text class='register' x='51' y='54'>{{controlSignals.C2 ?1:0}}{{controlSignals.C1 ?1:0}}{{controlSignals.C0 ?1:0}}</text>
        
            <line class='connection' x1='45' y1='63' x2='45' y2='66' 
                :class="{write: controlSignals.C0 || controlSignals.C1 || controlSignals.C2}" />
            <line class='connection' x1='69' y1='63' x2='69' y2='73' 
                :class="{write: controlSignals.C0 || controlSignals.C1 || controlSignals.C2}" />
        
        
            <line class='connection' x1='30' y1='71' x2='37' y2='71' 
                :class="{read: controlSignals.X}" />
        
            <rect class='register' x='37' y='66' width='15' height='10' 
                :class="{read: controlSignals.X}" />
            <text class='register' x='38' y='73'>X</text>
        
        
            <line class='connection' x1='30' y1='78' x2='62' y2='78' 
                :class="{read: controlSignals.Y}" />
        
            <rect class='register' x='62' y='73' width='15' height='10' 
                :class="{read: controlSignals.Y}" />
            <text class='register' x='63' y='80'>Y</text>
        
        
            <line class='connection' x1='30' y1='91' x2='22' y2='91' 
                :class="{read: controlSignals.S}" />
        
            <rect class='register' x='2' y='86' width='20' height='10' 
                :class="{read: controlSignals.S}" />
            <text class='register' x='3' y='93'>SAR</text>
        
        
            <line class='connection' x1='30' y1='91' x2='47' y2='91' 
                :class="{write: controlSignals.Dw, read: controlSignals.Dr}" />
        
            <rect class='register' x='47' y='86' width='20' height='10' 
                :class="{write: controlSignals.Dw, read: controlSignals.Dr}" />
            <text class='register' x='48' y='93'>SDR</text>

            <rect class='register' x='2' y='97' width='65' height='10'
                :class='{write: controlSignals.W, read: controlSignals.R}' />
            <text class='register' x='25' y='104'>Memory</text>
        
            <line class='bus' x1='30' y1='6' x2='30' y2='92' />
        
        </svg>    
        </div>
    `,
    props: {
        controlSignals: {
            default: _ => {
                return {}
            }
        }
    },
    methods: {}
})