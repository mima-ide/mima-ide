import Vue from './vue.esm.js'
import parallaxEffect from './parallax.js'

Vue.component('ui-menu', {
    template: `<span class="ui-menu" :style="[parallax]">
        <slot>
            <ui-menu-section v-for="item in items" :key="item.id" :items="item.items" :title="item.title"></ui-menu-section>
        </slot>
    </span>`,
    props: {
        items: {
            default: _ => {return []}
        }
    },
    mixins: [parallaxEffect]

})

Vue.component('ui-menu-item', {
    template: `<span class="ui-menu-item" @click="clicked" :class='{disabled: disabled}'>
        <ui-spot :x="pointerX" :y="pointerY" />
        <slot>
            <ui-icon v-if="icon != null" :icon="icon"></ui-icon>
            <ui-text :text="title"></ui-text>
        </slot>
    </span>`,
    props: ['title', 'icon', 'callback', 'value', 'disabled'],
    mixins: [parallaxEffect],
    methods: {
        clicked: function(evt) {
            if (this.disabled == true) {
                evt.stopPropagation()
                return
            }
            if (this.callback) {
                this.callback(this.value, this.$el)
            }
        }
    }

})

Vue.component('ui-menu-separator', {
    template: `<span class="ui-menu-separator">
    </span>`,
    props: [],

})

Vue.component('ui-menu-section', {
    template: `<span class="ui-menu-section">
        <slot>
            <ui-text v-if="title != ''">{{title}}</ui-text>
            <ui-menu-item v-for="item in items" :key="item.id" :icon="item.icon" :title="item.title" :callback="item.callback" :value="item.value" :disabled="item.disabled"></ui-menu-item>
        </slot>
    </span>`,
    props: {
        title: {
            default: ""
        },
        items: {
            default: _ => {return []}
        }
    },

})

export default {

    open: function(el, menu, options = {}) {

        options.menu = menu
        options.anchor = el
        el.dispatchEvent(new CustomEvent('menu', {
            detail: options,
            bubbles: true
        }))

    },
    contextmenu: function(el, evt, menu, options = {}) {

        let pos = { left: evt.clientX - 30, top: evt.clientY - 20}
        options.menu = menu
        options.anchor = el
        options.position = pos
        el.dispatchEvent(new CustomEvent('menu', {
            detail: options,
            bubbles: true
        }))

    }

}