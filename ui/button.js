import Vue from './vue.esm.js'
import parallaxEffect from './parallax.js'

Vue.component('ui-button', {
    template: `<span class="ui-button" :style="[parallax]" :title="tip" @click="clicked">
        <ui-spot :x="pointerX" :y="pointerY" />
        <slot>
            <ui-icon v-if="icon != null" :icon="icon"></ui-icon>
            <ui-text v-if="title != null" :text="title"></ui-text>
        </slot>
    </span>`,
    props: ['title', 'icon', 'tip', 'action', 'value'],
    mixins: [parallaxEffect],
    methods: {
        clicked: function(evt) {
            if (this.action == null) { return; }

            this.$emit('action', {
                action: this.action, 
                value: this.value
            })
        }
    }

})
