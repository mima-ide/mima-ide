import Vue from './vue.esm.js'

Vue.component('ui-content', {
    template: `
        <div class='ui-content'>
            <slot></slot>
        </div>
    `,
    props: {
    },
    methods: {}
})