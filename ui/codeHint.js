import Vue from './vue.esm.js'

Vue.component('ui-code-hint', {
    template: `<span class="ui-code-hint">
        <span :is="type" :data="data"></span>
    </span>`,
    props: ['type', 'data'],
    methods: {
        
    }

})

Vue.component('ui-code-hint-view', {
    template: `<span class="ui-code-hint-view">
        <slot></slot>
        <ui-code-hint 
            v-if="type != null"
            :type="type"
            :data="data"
            :style="pos"
        ></ui-code-hint>
        
    </span>`,
    data: _ => {
        return {
            type: null,
            data: {},
            position: {
                left: 0,
                top: 0
            }
        }
    },
    computed: {
        pos() {
            return {
                top: `${this.position.top}px`,
                left: `${this.position.left}px`,
            }
        }
    },
    mounted() {

        this.$el.addEventListener('code-hint', evt => {

            this.type = evt.detail.type ?? null
            this.data = evt.detail.data ?? {}
            this.position = evt.detail.position ?? {top: 0, left: 0}

        })

        this.$el.addEventListener('close-code-hint', evt => {

            this.type = null
            this.data = {}

        })

    }
})

export default {

    open(el, type, data, position) {

        el.dispatchEvent(new CustomEvent('code-hint', {
            detail: {
                type: type,
                data: data,
                position: position
            },
            bubbles: true
        }))

    },

    close(el) {

        el.dispatchEvent(new CustomEvent('close-code-hint', {
            bubbles: true
        }))

    },

}