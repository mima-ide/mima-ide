import Vue from './vue.esm.js'
import microcode from '../res/microcode.js'

Vue.component('ui-code-hint-microcode', {
    template: `<span class="ui-code-hint-microcode">
        <strong v-if="data.description.trim() != ''">
            {{data.description}}
        </strong>
        <em v-if="data.description.trim() == ''">
            (No Description available)
        </em>
        <table>
            <tr>
                <th>A<sub>r</sub></th>
                <th>A<sub>w</sub></th>
                <th>X</th>
                <th>Y</th>

                <th>Z</th>
                <th>E</th>
                <th>P<sub>r</sub></th>
                <th>P<sub>w</sub></th>

                <th>I<sub>r</sub></th>
                <th>I<sub>w</sub></th>
                <th>D<sub>r</sub></th>
                <th>D<sub>w</sub></th>

                <th>S</th>
                <th>C<sub>2</sub></th>
                <th>C<sub>1</sub></th>
                <th>C<sub>0</sub></th>

                <th>R</th>
                <th>W</th>
            </tr>
            <tr>
                <td>{{sig.Ar?1:0}}</td>
                <td>{{sig.Aw?1:0}}</td>
                <td>{{sig.X?1:0}}</td>
                <td>{{sig.Y?1:0}}</td>

                <td>{{sig.Z?1:0}}</td>
                <td>{{sig.E?1:0}}</td>
                <td>{{sig.Pr?1:0}}</td>
                <td>{{sig.Pw?1:0}}</td>

                <td>{{sig.Ir?1:0}}</td>
                <td>{{sig.Iw?1:0}}</td>
                <td>{{sig.Dr?1:0}}</td>
                <td>{{sig.Dw?1:0}}</td>

                <td>{{sig.S?1:0}}</td>
                <td>{{sig.C2?1:0}}</td>
                <td>{{sig.C1?1:0}}</td>
                <td>{{sig.C0?1:0}}</td>

                <td>{{sig.R?1:0}}</td>
                <td>{{sig.W?1:0}}</td>
            </tr>
        </table>
        <span class='next-line'>Next line: {{nextLine}}</span>
    </span>`,
    props: ['data'],
    computed: {
        nextLine() {
            return '0x' + (this.data.code & 0xff).toString(16)
        },
        sig() {
            return microcode.signalsFromMicrocode(this.data.code)
        }
    }
})