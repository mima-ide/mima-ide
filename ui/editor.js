import Vue from './vue.esm.js'

Vue.component('ui-editor', {
    template: `
        <div class='ui-editor'>
            <div ref='editor' class='editor'></div>
        </div>
    `,
    props: {
    },
    methods: {},
    mounted: function() {
        let cm = CodeMirror(this.$refs.editor, {
            lineNumbers: true,
            theme: "shadowfox",
            mode: "mimasm",
            // readOnly: true,
            firstLineNumber: 0,
            lineNumberFormatter: n => {
                const num = ("00000" + n.toString(16)).slice(-5)
                return `0x${num}`
            },
            gutters: ['symbols', 'CodeMirror-linenumbers', 'debug-infos', 'breakpoints'],
            value: ""
        })
    
    }
})