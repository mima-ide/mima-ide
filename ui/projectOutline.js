import Vue from './vue.esm.js'
import lfs from '../res/lfs.js'
import dialog from './dialog.js'
import make from '../res/assembler/make.js'

Vue.component('ui-project-outline', {
    template: `
        <div class='ui-project-outline'>
            <ui-outline :items="outlineItems" :menuFunction="menuFunction" :selected="selected" @clicked='clicked'></ui-outline>
        </div>
    `,
    data: _ => {return {
        items: [],
        
    }},
    props: {
        root: {
            default: '/'
        },
        selected: {
            default: ''
        },
        architecture: {
            default: 'mima'
        },
    },
    watch: {
        root: function() {
            this.items = []
            this.updateChildren(this.root)
        }
    },
    computed: {
        outlineItems: function() {
            return this.items
        }
    },
    methods: {

         getNodeForPath: function(path) {
            // Get node for specific path
            if (path == this.root) {
                return this
            }

            let fragments = path.split('/')
            let items = this.items
            for (let i = 1; i <= fragments.length; ++i) {
                for (let item of items) {
                    if (item.id == fragments.slice(0,i + 1).join('/')) {
                        items = item.items
                        ++i;
                        if (i >= fragments.length) {
                            return item
                        }
                    }
                }
            }
            return null
        },

        findPosition: function(list, item) {

            for (let i = 0; i < list.length; ++i) {
                if (item <= list[i]) {
                    return i
                }
            }

            return list.length

        },

        updateChildren: function(path) {

            let parentNode = this.getNodeForPath(path)

            if (parentNode == null) { return }

            lfs.ready(async _ => {
                let isDir = await lfs.isDir(path)
                if (!isDir) { return }
                let children = await lfs.list(path)
                let childrenPaths = Object.keys(children).map(c => children[c])
                let currentChildren = parentNode.items.map(i => i.id)

                let childrenToRemove = currentChildren.filter(c => !childrenPaths.includes(c))
                let childrenToAdd = childrenPaths.filter(c => !currentChildren.includes(c))

                childrenToRemove.forEach(c => {
                    let index = parentNode.items.map(i => i.id).indexOf(c)
                    parentNode.items.splice(index, 1)
                })

                childrenToAdd.forEach(c => {
                    let index = this.findPosition(parentNode.items.map(i => i.id), c)
                    parentNode.items.splice(index, 0, {id: c, title: lfs.basename(c), items: []})
                })

                childrenPaths.forEach(c => {
                    this.updateChildren(c)
                })


            })

        },

        recalculateChildren: function(path) {

            return new Promise((resolve) => {

                lfs.ready(async _ => {
                    let items = await lfs.list(path)

                    let res = []
                    Object.keys(items).forEach(name => {
                        let path = items[name]
                        let node = {title: name, id: path, items: []}
                        res.push(node)

                        lfs.isDir(path).then(isDir => {
                            if (isDir) {
                                this.recalculateChildren(path).then(i => i.forEach(_ => node.items.push(_)))
                            }
                        })
                        

                    })

                    resolve(res)
                })

            })
        },
        menuFunction: async function(id) {

            let isDir = await lfs.isDir(id)

            if (isDir) {
                return [{items: [
                    {title: "New File", icon: "fas-file", callback: async _ => {

                        let r = await dialog.saveFileDialog(this.$el, {defaultLocation: id}).catch(_ => {})
                        let name = r.value

                        if (!lfs.validPath(name)) {
                            dialog.alert(this.$el, "Project", `Invalid Path ${name}`)
                            return
                        }

                        let exists = await lfs.exists(name)

                        if (exists) {
                            dialog.alert(this.$el, "Project", `File ${name} exists already`)
                            return
                        }

                        lfs.put(name, "").catch(e => {
                            dialog.alert(this.$el, "Project", e)
                        })

                    }},
                    {title: "New Directory", icon: "fas-folder", callback: _ => {

                        dialog.input(this.$el, "Project", "Name").then(r => {

                            if (!lfs.validName(r.value)) {
                                dialog.alert(this.$el, "Project", "Invalid Name")
                                return
                            }

                            let name = `${id}/${r.value}`
                            lfs.mkdir(name).catch(e => {
                                dialog.alert(this.$el, "Project", e)
                            })

                        }, _ => {})

                    }}
                ]}, {items: [
                    {title: "Delete Directory", icon: "fas-trash", callback: _ => {

                        dialog.alert(this.$el, "Project", `Delete directory ${id}?`, {
                            buttons: [{title: "Delete", action: "delete"}, {title: "Cancel", action: "cancel"}]
                        }).then(r => {
                            if (r.action == 'delete') {
                                lfs.rmdir(id).catch(e => {
                                    dialog.alert(this.$el, "Project", e)
                                })
                            }
                        }, _ => {})

                        
                    }}
                ]}]
            } else {

                const downloadOptions = [
                    {title: "Download File", icon: "fas-file-download", callback: _ => {
                        if (lfs.extension(id) == 'bin') {
                            lfs.downloadBinary(id)
                        } else {
                            lfs.download(id)
                        }
                    }},
                ]

                const mimaExportOptions = [
                    {title: "Export standard Mima", icon: "fas-file-code", callback: async _ => {

                        const filename = id + ".prep.tmp"
                        await (new make(this.architecture)).make(id, make.PREPROCESS, _ => filename)
                        await lfs.download(filename, lfs.basename(id))
                        await lfs.rm(filename)

                    }},
                    {title: "Export Memory Map", icon: "fas-file", callback: async _ => {
                        this.exportMemoryMap(id)
                    }},
                    {title: "Export Bytecode", icon: "fas-file", callback: async _ => {
                        this.exportBytecode(id)
                    }}
                ]

                const manageOptions = [
                    {title: "Rename File", icon: "fas-pen", callback: _ => {

                        let basename = lfs.basename(id)

                        dialog.input(this.$el, "Project", `New name for ${basename}`).then(r => {

                            if (!lfs.validName(r.value)) {
                                dialog.alert(this.$el, "Project", "Invalid Name")
                                return
                            }

                            let parent = lfs.pathname(id)
                            lfs.mv(id, `${parent}/${r.value}`).catch(e => {
                                dialog.alert(this.$el, "Project", e)
                            })

                        }, _ => {})

                    }},
                    {title: "Duplicate File", icon: "fas-copy", callback: async _ => {

                        const {value: newName} = await dialog.saveFileDialog(this.$el, {defaultLocation: lfs.pathname(id)})

                        const contents = await lfs.get(id)
                        await lfs.put(newName, contents)

                    }},
                    {title: "Delete File", icon: "fas-trash", callback: _ => {

                        dialog.alert(this.$el, "Project", `Delete file ${id}?`, {
                            buttons: [{title: "Delete", action: "delete"}, {title: "Cancel", action: "cancel"}]
                        }).then(r => {
                            if (r.action == 'delete') {
                                lfs.rm(id).catch(e => {
                                    dialog.alert(this.$el, "Project", e)
                                })
                            }
                        }, _ => {})
                    }}
                ]

                const type = this.fileType(lfs.extension(id))

                if (type == "mima") {
                    return [{items: downloadOptions}, {items: mimaExportOptions}, {items: manageOptions}]
                } else {
                    return [{items: downloadOptions}, {items: manageOptions}]
                }

                
            }
        },

        clicked(data) {

            this.$el.dispatchEvent(new CustomEvent('switch-file', {
                detail: data,
                bubbles: true
            }))


        },

        fileType(extension) {

            return {
                mima: "mima",
                mimasm: "mima",
                mimax: "mima",
                mm: "micro",
                micro: "micro"
            }[extension] ?? extension

        },

        async exportMemoryMap(file) {
            const filename = file + ".prep.tmp"
            await (new make(this.architecture)).make(file, make.BUILD, _ => filename)
            await lfs.download(filename, lfs.basename(file) + '.map')
            await lfs.rm(filename)
        },

        async exportBytecode(file) {

            const filename = file + ".prep.tmp.bin"
            await (new make(this.architecture)).make(file, make.BYTECODE, _ => filename)
            await lfs.downloadBinary(filename, lfs.basename(file) + '.bin')
            await lfs.rm(filename)

        },

    },
    mounted: function() {

        this.updateChildren(this.root)

        lfs.watch((update, path) => {
            this.updateChildren(this.root)
        })

    }
})