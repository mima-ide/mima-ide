import Vue from './vue.esm.js'
const Remarkable = window.remarkable.Remarkable

Vue.component('ui-markdown-view', {
    template: `<span class="ui-markdown-view" v-html="renderedText">
    </span>`,
    data: _ => {
        return {
            code: ""
        }
    },
    props: {
        url: {
            default: null
        },
        followLinks: {
            default: false
        },
    },
    computed: {

        renderedText() {

            return (new Remarkable()).render(this.code)

        }

    },

    watch: {

        url() {
            this.update()
        }

    },
    methods: {

        async update() {

            if (this.url == null) { return }
            const response = await fetch(this.url)
            this.code = await response.text()

        }

    },

    mounted: function() {
        this.update()

        this.$el.addEventListener('click', evt => {

            if (evt.target.tagName.toLowerCase() == 'a' && !this.followLinks) {
                evt.preventDefault()
                evt.stopPropagation()
                this.$emit('navigate', evt.target.href)
            }

        })

    }

})
