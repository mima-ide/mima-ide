import Vue from './vue.esm.js'
import lfs from '../res/lfs.js'
import dialog from './dialog.js'
import rfs from '../res/rfs.js'
import menu from './menu.js'
import findMenu from './findMenu.js'

Vue.component('ui-memory-edit-dialog', {
    template: `<ui-dialog class='ui-memory-edit-dialog'>
        <template v-slot:title>
            <ui-text :text="title"></ui-text>
        </template>
        <ui-dialog-view>
            <span class='ui-form-composer'>
                <input type='text' ref='valueInput' v-model="value"></input>
                <ui-button icon="fas-chevron-down" @click.native="openSymbolMenu"></ui-button>
            </span>
        </ui-dialog-view>
        <template v-slot:buttons>
            <ui-button title="Save" @click.native="save" class='active'></ui-button>
            <ui-button title="Cancel" @click.native="cancel"></ui-button>
            <div class='ui-div'></div>
        </template>
    </ui-dialog>
    `,
    data: _ => {
        return {
            value: {
                default: 0
            }
        }
    },
    props: {
        options: {
            default: _ => {return {}}
        }
    },
    computed: {
        title() {
            return `Edit ${this.options.name}`
        },
        symbolMenu() {

            return this.options.symbols.map(symb => {
                return {
                    title: symb.name,
                    id: symb.address,
                    callback: _ => {
                        this.value = '0x' + symb.address.toString(16)
                    }
                }
            })

        }
    },
    methods: {
        cancel: function() {
            this.clearTraps()
            this.$emit('cancel-dialog')
        },
        save: function() {

            this.clearTraps()
            this.$emit('accept-dialog', {action: 'save', value: parseInt(this.value)})
        },
        clearTraps: function() {
            document.removeEventListener('keydown', this.keyTrap, {capture: true})
        },
        keyTrap: function(evt) {
            if (evt.key.toLowerCase() == "enter") {
                evt.preventDefault()
                evt.stopPropagation()
                this.save()
            } else if (evt.key.toLowerCase() == "escape") {
                evt.preventDefault()
                evt.stopPropagation()
                this.cancel()
            }
        },
        openSymbolMenu(evt) {

            findMenu.open(evt.target, this.symbolMenu)

        },
    },
    mounted: function() {
        this.value = '0x' + (this.options.value ?? 0).toString(16)
        this.$refs.valueInput.focus()
        document.addEventListener('keydown', this.keyTrap, {capture: true})
        this.$el.addEventListener('dialog-open', evt => {
            this.clearTraps()
        })
        this.$el.addEventListener('dialog-close', evt => {
            document.addEventListener('keydown', this.keyTrap, {capture: true})
        })
    }

})

