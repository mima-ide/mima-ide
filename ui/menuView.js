import Vue from './vue.esm.js'

Vue.component('ui-menu-view', {
    template: `
        <div class='ui-menu-view' @menu="openMenu">
            <slot></slot>
            <ui-menu :items="menu" :style="[position]" v-if="menu != null" ref="menu"></ui-menu>
        </div>
    `,
    data: _ => {
        return {
            menu: null,
            offsetPosition: {
                x: 0,
                y: 0
            },
            callback: _ => {}
        }
    },
    computed: {
        position: function() {
            return {
                left: `${this.offsetPosition.x}px`,
                top: `${this.offsetPosition.y}px`
            }
        }
    },
    props: {
    },
    methods: {
        openMenu: function(evt) {

            this.menu = evt.detail.menu
            this.callback = evt.detail.callback || (_ => {})

            let co = evt.detail.position
                ? evt.detail.position
                : this.cumulativeOffset(evt.detail.anchor)
            
            this.offsetPosition.x = co.left
            this.offsetPosition.y = co.top

            document.addEventListener('click', evt => {
                this.resetMenu()
            }, {once: true, capture: true})

            this.$el.addEventListener('close-menu', evt => {
                this.resetMenu()
            })

            window.setTimeout(_ => {

                if (co.left + this.$refs.menu.$el.offsetWidth > this.$el.offsetWidth) {
                    this.offsetPosition.x = this.$el.offsetWidth - this.$refs.menu.$el.offsetWidth - 5
                }

            }, 2)

        },
        cumulativeOffset: function(element) {
            let top = 0, left = 0;
            do {
                top += element.offsetTop  || 0;
                left += element.offsetLeft || 0;
                element = element.offsetParent;
            } while(element)

            return {
                top: top,
                left: left
            }
        },
        // menuItemClicked: function(data) {
        //     this.callback(data)
        //     this.resetMenu();
        // },
        resetMenu: function() {
            this.menu = null
            this.callback = _ => {}
        }
    }
})