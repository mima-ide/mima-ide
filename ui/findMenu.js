import Vue from './vue.esm.js'
import parallaxEffect from './parallax.js'

Vue.component('ui-find-menu', {
    template: `<span class="ui-find-menu" :style="[parallax]">
        
        <input type="text" v-model="term" ref='input' @keydown='keydown' />
        <div style="flex-grow: 1; position: relative">
            <div class="ui-scroll">
                <ui-menu-item 
                    v-for="item in foundItems" 
                    :key="item.id" 
                    :title="item.title"
                    :icon="item.icon"
                    :value="item.value"
                    :callback="item.callback"
                    ></ui-menu-item>
            </div>
        </div>
    </span>`,
    data: _ => {
        return {
            term: ""
        }
    },
    props: {
        items: {
            default: _ => {return []}
        }
    },
    computed: {
        foundItems() {

            return this.items.filter(i => {
                return i.title.toLowerCase().indexOf(this.term.toLowerCase()) != -1
            })

        }
    },
    methods: {
        keydown(evt) {
            if (evt.key.toLowerCase() == "enter") {
                evt.preventDefault()
                const foundItem = this.foundItems[0]
                if (foundItem != null) {
                    foundItem.callback(foundItem.value, this.$el)
                }
                this.$el.dispatchEvent(new CustomEvent('close-menu', {
                    bubbles: true
                }))

            } else if (evt.key.toLowerCase() == "escape") {
                evt.preventDefault()
                this.$el.dispatchEvent(new CustomEvent('close-menu', {
                    bubbles: true
                }))
            }
        }
    },
    mixins: [parallaxEffect],
    mounted: function() {
        this.$refs.input.focus()
    }

})

Vue.component('ui-find-menu-view', {
    template: `
        <div class='ui-find-menu-view' @find-menu="openMenu">
            <slot></slot>
            <ui-find-menu :items="menu" :style="[position]" v-if="menu != null" ref="menu"></ui-find-menu>
        </div>
    `,
    data: _ => {
        return {
            menu: null,
            offsetPosition: {
                x: 0,
                y: 0
            },
            callback: _ => {}
        }
    },
    computed: {
        position: function() {
            return {
                left: `${this.offsetPosition.x}px`,
                top: `${this.offsetPosition.y}px`
            }
        }
    },
    props: {
    },
    methods: {
        openMenu: function(evt) {

            this.menu = evt.detail.menu
            this.callback = evt.detail.callback || (_ => {})

            let co = evt.detail.position
                ? evt.detail.position
                : this.cumulativeOffset(evt.detail.anchor)
            
            this.offsetPosition.x = co.left
            this.offsetPosition.y = co.top

            document.addEventListener('click', evt => {
                this.resetMenu()
            }, {once: true, capture: true})

            this.$el.addEventListener('close-menu', evt => {
                this.resetMenu()
            })

            window.setTimeout(_ => {

                if (co.left + this.$refs.menu.$el.offsetWidth > this.$el.offsetWidth) {
                    this.offsetPosition.x = this.$el.offsetWidth - this.$refs.menu.$el.offsetWidth - 5
                }

            }, 2)

        },
        cumulativeOffset: function(element) {
            let top = 0, left = 0;
            do {
                top += element.offsetTop  || 0;
                left += element.offsetLeft || 0;
                element = element.offsetParent;
            } while(element)

            return {
                top: top,
                left: left
            }
        },
        resetMenu: function() {
            this.menu = null
            this.callback = _ => {}
        }
    }
})

export default {

    open: function(el, menu, options = {}) {

        options.menu = menu
        options.anchor = el
        el.dispatchEvent(new CustomEvent('find-menu', {
            detail: options,
            bubbles: true
        }))

    },

}