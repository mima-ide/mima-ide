import Vue from './vue.esm.js'

Vue.component('ui-microcode-assistant', {
    template: `
        <div class='ui-microcode-assistant'>
            <ui-vsplitter>
                <template v-slot:top>
                    <ui-microcode-memory-editor 
                        :memory="simulator.microcode" 
                        :instructionPointer="simulator.registers.MIAR ?? 0"
                        :instructionRegister="simulator.registers.SDR1"
                        ></ui-microcode-memory-editor>
                </template>

                <template v-slot:bottom>
                    <div style='position: absolute; top: 0; left: 0; right: 0; bottom: 0; overflow: hidden;'>
                        <ui-schema :controlSignals="simulator.controlSignals ?? {}"></ui-schema>
                    </div>
                </template>

            </ui-vsplitter>
            
           
        </div>
    `,
    props: {
        simulator: {
            default: null
        }
    },
    computed: {
    },
    methods: {}
})