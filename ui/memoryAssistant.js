import Vue from './vue.esm.js'

Vue.component('ui-memory-assistant', {
    template: `
        <div class='ui-memory-assistant'>
            <ui-vsplitter>
                <template v-slot:top>
                    <ui-memory-editor 
                        default-section=".data"
                        :memory="simulator.memory" 
                        :sections="simulator.sections" 
                        :symbols="simulator.symbols"
                        :instructionPointer="simulator.instructionPointer"
                        :architecture="architecture"
                        :breakpoints="simulator.breakpoints"
                        ></ui-memory-editor>
                </template>
                <template v-slot:bottom>
                    <ui-memory-editor
                        default-section=".text"
                        shouldFollowIP="true"
                        :memory="simulator.memory" 
                        :sections="simulator.sections" 
                        :symbols="simulator.symbols"
                        :instructionPointer="simulator.instructionPointer"
                        :architecture="architecture"
                        :breakpoints="simulator.breakpoints"
                        ></ui-memory-editor>
                </template>
            </ui-vsplitter>
        </div>
    `,
    props: {
        simulator: {
            default: null
        },
        architecture: {
            default: "mima"
        }
    },
    methods: {}
})