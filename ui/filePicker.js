import Vue from './vue.esm.js'
import lfs from '../res/lfs.js'
import dialog from './dialog.js'
import menu from './menu.js'

Vue.component('ui-file-picker', {
    template: `
        <div class='ui-file-picker'>
            <div class='ui-file-picker-toolbar'>
                <ui-button icon='fas-arrow-up' tag='Folder up' @click.native='moveUp'></ui-button>
                <ui-button icon='fas-folder-plus' tag='New Folder' @click.native='addFolder'></ui-button>
                <ui-button class='active ui-file-picker-location' :title='currentFolder' :icon='currentFolderIcon' @click.native='openLocationSwitcher'></ui-button>
                <span class='ui-div'></span>
                <!--<ui-button icon='fas-th-large' tag='Icons'></ui-button>
                <ui-button icon='fas-th-list' tag='List'></ui-button>-->
            </div>
            <div class='ui-file-picker-content'>
                <div class='ui-scroll'>
                    <ui-list :items='files' :selected='selectedFiles' @ui-clicked='fileSelected' item-style='ui-list-item-simple' class='no-animation'></ui-list>
                </div>
            </div>
        </div>
    `,
    data: _ => {
        return {
            location: '/',
            files: [],
            selectedFiles: []
        }
    },
    watch: {
        location: function() {
            this.loadFiles(this.location)
            this.selectedFiles = [this.location]
        },
        selectedFiles: function() {
            this.$emit("input", this.selectedFiles)
        }
    },
    props: {
        value: {
            default: _=> {return []}
        },
        foldersOnly: {
            default: false
        },
        defaultLocation: {
            default: "/"
        }
    },
    computed: {
        currentFolder: function() {
            return this.location == '/'
                ? 'Root'
                : lfs.basename(this.location)
        },
        currentFolderIcon: function() {
            return this.location == '/'
                ? 'fas-database'
                : 'fas-folder'
        }
    },
    methods: {
        loadFiles: function(path) {
            lfs.ready(async _ => {
                let items = await lfs.list(path)
                let isDir = []

                for (let f of Object.keys(items)) {
                    isDir[f] = await lfs.isDir(items[f])
                }

                this.updateFilesList(Object.keys(items).map(f => {
                    return {
                        id: items[f],
                        title: f,
                        icon: isDir[f] ? 'fas-folder' : 'fas-file',
                        isDir: isDir[f],
                        enabled: !this.foldersOnly || isDir[f]
                    }
                }))

            })
        },
        findPosition: function(list, item) {

            for (let i = 0; i < list.length; ++i) {
                if (item.id <= list[i].id) {
                    return i
                }
            }

            return list.length

        },
        updateFilesList: function(newList) {
            let removed = this.files.filter(i => !newList.includes(i))
            let added = newList.filter(i => !this.files.includes(i))

            removed.forEach(i => {
                let index = this.files.indexOf(i)
                this.files.splice(index, 1)
            })

            added.forEach(i => {
                let index = this.findPosition(this.files, i)
                this.files.splice(index, 0, i)
            })

        },
        moveUp: function() {
            this.location = lfs.pathname(this.location)
        },
        fileSelected: function(path) {

            lfs.ready(async _ => {

                let isDir = await lfs.isDir(path)
                if (isDir) {
                    this.location = path
                } else {
                    if (!this.foldersOnly) {
                        this.selectedFiles = [path]
                    }
                }

            })

        },
        addFolder: function() {
            dialog.input(this.$el, "New Folder", "Folder Name").then(r => {

                if (!lfs.validName(r.value)) {
                    dialog.alert(this.$el, "New Folder", "Invalid Name")
                    return
                }

                let name = `${this.location}/${r.value}`
                lfs.mkdir(name).catch(e => {
                    dialog.alert(this.$el, "New Folder", e)
                })

            }, e => {})
        },
        openLocationSwitcher: function(evt) {
            let parents = [this.location]
            let current = this.location
            while (current != '/') {
                current = lfs.pathname(current)
                parents.splice(0, 0, current)
            }
            menu.open(evt.currentTarget, [{
                items: parents.map(i => {
                    return {
                        title: i == '/' ? 'Root' : lfs.basename(i),
                        id: i,
                        value: i,
                        icon: i == '/' ? 'fas-database' : 'fas-folder',
                        callback: path => {
                            this.location = path
                        }
                    }
                })
            }])
        }
    },
    mounted: function(){
        this.selectedFiles = this.value
        this.location = this.defaultLocation
        this.loadFiles(this.location)
        lfs.watch(_ => {this.loadFiles(this.location)})
    }
})