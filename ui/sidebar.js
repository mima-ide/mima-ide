import Vue from './vue.esm.js'

Vue.component('ui-sidebar', {
    template: `
        <div class='ui-sidebar'>
            <div class='ui-sidebar-content'><slot></slot></div>
        </div>
    `,
    props: {
        title: {
            type: String,
            default: ""
        }
    },
    methods: {}
})