import lfs from '../res/lfs.js'
import dialog from './dialog.js'
import menu from './menu.js'
import Vue from './vue.esm.js'

Vue.component('ui-project-settings', {
    template: `
        <div class='ui-project-settings'>

            <div class='ui-scroll' style="padding: 0 20px;">

                <div class='ui-form'>
                    <ui-text class='ui-form-heading' text="Build"></ui-text>
                    <div class='ui-form-entry'>
                        <ui-text text="Architecture"></ui-text>
                        <ui-button :title="architectureName" class='active' @click.native="openArchitectureMenu"></ui-button>
                    </div>
                    <div class='ui-form-entry'>
                        <ui-text text="Build Target"></ui-text>
                        <ui-button :title="buildTargetName" class='active' @click.native="openBuildTargetMenu"></ui-button>
                    </div>
                    <div class='ui-form-entry'>
                        <ui-text text="Entry File"></ui-text>
                        <ui-button 
                            icon='fas-folder' 
                            :title="entryFile ?? 'Select File...'" 
                            class='active' 
                            @click.native="selectEntryFile"></ui-button>
                    </div>

                    <ui-text class='ui-form-heading' text="Custom Architecture"></ui-text>
                    <div class='ui-form-entry'>
                        <ui-text text="Opcode File"></ui-text>
                        <ui-button 
                            icon='fas-folder' 
                            :title="opcodeFile ?? 'Select File...'" 
                            class='active' 
                            @click.native="selectOpcodeFile"></ui-button>
                    </div>
                    <div class='ui-form-entry'>
                        <ui-text text="Microcode File"></ui-text>
                        <ui-button 
                            icon='fas-folder' 
                            :title="microcodeFile ?? 'Select File...'" 
                            class='active' 
                            @click.native="selectMicrocodeFile"></ui-button>
                    </div>
                </div>
            </div>
        </div>
    `,
    data: _ => {
        return {
            architecture: 'mima',
            buildTarget: 'build-project',
            entryFile: 'program.mima',
            opcodeFile: null,
            microcodeFile: null,
        }
    },
    props: {
        title: {
            default: "Project Settings"
        },
        architectures: {
            default: []
        },
        projectRoot: {
            default: '/'
        },
    },
    computed: {
        architectureMenu() {
            return [{items: this.architectures.map(arch => {
                return {
                    id: arch.id,
                    title: arch.title,
                    callback: _ => {
                        this.architecture = arch.id
                        this.saveChanges()
                    }
                }
            })}]
        },
        architectureName() {
            return (this.architectures.filter(arch => arch.id == this.architecture)[0] ?? {title: '(Unknown)'}).title
        },
        dataExport() {
            return JSON.stringify({
                architecture: this.architecture,
                buildTarget: this.buildTarget,
                entryFile: this.entryFile,
                opcodeFile: this.opcodeFile,
                microcodeFile: this.microcodeFile,
            })
        },
        projectFile() {
            return `${this.projectRoot}/project.json`
        },
        buildTargetMenu() {
            return [
                {items: [
                    {id: "build-project", title: "Build Project", callback: _ => {
                        this.buildTarget = 'build-project'
                        this.saveChanges()
                    }},
                    {id: "build-file", title: "Build File", callback: _ => {
                        this.buildTarget = 'build-file'
                        this.saveChanges()
                    }}
                ]}
            ]
        },
        buildTargetName() {
            return {
                'build-project': "Build Project",
                'build-file': "Build File"
            }[this.buildTarget]
        },
    },
    watch: {
        projectFile() {
            this.loadData()
        }
    },
    methods: {
        close() {
            this.$emit('accept-dialog', {action: 'upate', value: {}})
        },
        selectEntryFile() {
            dialog.openFileDialog(this.$el, {defaultLocation: this.projectRoot}).then(ret => {
                const filename = lfs.relative(ret.value ?? "", this.projectRoot)
                if (filename == null) {
                    dialog.alert(this.$el, "Project Settings", "Entry file must be inside Project Root")
                    return
                }
                if (lfs.extension(filename) != 'mima') {
                    dialog.alert(this.$el, "Project Settings", "Entry file must be .mima file")
                    return
                }
                this.entryFile = filename
                this.saveChanges()
            }, _ => {})
        },
        selectOpcodeFile() {
            dialog.openFileDialog(this.$el, {defaultLocation: this.projectRoot}).then(ret => {
                const filename = lfs.relative(ret.value ?? "", this.projectRoot)
                if (filename == null) {
                    dialog.alert(this.$el, "Project Settings", "Opcode file must be inside Project Root")
                    return
                }
                if (lfs.extension(filename) != 'json') {
                    dialog.alert(this.$el, "Project Settings", "Opcode file must be .json file")
                    return
                }
                this.opcodeFile = filename
                this.saveChanges()
            }, _ => {})
        },
        selectMicrocodeFile() {
            dialog.openFileDialog(this.$el, {defaultLocation: this.projectRoot}).then(ret => {
                const filename = lfs.relative(ret.value ?? "", this.projectRoot)
                if (filename == null) {
                    dialog.alert(this.$el, "Project Settings", "Microcode file must be inside Project Root")
                    return
                }
                if (lfs.extension(filename) != 'mm') {
                    dialog.alert(this.$el, "Project Settings", "Microcode file must be .mm file")
                    return
                }
                this.microcodeFile = filename
                this.saveChanges()
            }, _ => {})
        },
        openArchitectureMenu(evt) {
            menu.open(evt.target, this.architectureMenu)
        },
        openBuildTargetMenu(evt) {
            menu.open(evt.target, this.buildTargetMenu)
        },
        saveChanges() {
            lfs.put(this.projectFile, this.dataExport)
        },
        async loadData() {
            const fileExists = await lfs.isFile(this.projectFile)

            let data = {}
            if (fileExists) { 
                const text = await lfs.get(this.projectFile)
                data = JSON.parse(text)
            }            
            

            this.architecture = data.architecture ?? "mima"
            this.buildTarget = data.buildTarget ?? "build-project"
            this.entryFile = data.entryFile ?? "program.mima"
            this.opcodeFile = data.opcodeFile ?? null
            this.microcodeFile = data.microcodeFile ?? null
        },
    },
    mounted: function() {
        this.loadData()
    }
})