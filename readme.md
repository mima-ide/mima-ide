# Mima IDE
This is a Open Source IDE for the Mima.


This IDE is still in alpha development, therefore, a lot of things might not work yet!

## About the Mima architecture

The Minimalmachine (Mima) is a conceptual Hardware architecture
developed by Prof. Tamim Asfour at the Karlsruhe Institute of Technology.
[More Information about the architecture and Reference Implementation](http://ti.ira.uka.de/Visualisierungen/Mima/)

## About the Mima IDE

This IDE is a project maintained by Daniel Ritz.

[Git Repo](https://gitlab.com/mima-ide/mima-ide)

[Issue Tracker](https://gitlab.com/mima-ide/mima-ide/-/issues)

This IDE strives for compatibility to other simulators by using the export feature
for Memory Map, Standard Mima Code or Bytecode. The extended macro and assembler directives
are most likely not supported by other simulators.

## What works?

- Projects and Files
- Mima simulator (only mima architecture)
- Mima assembler (assembles mimasm to Memory Map)
- Preprocessor Macros, File Imports, Automatic segment placement
- Step-by-step debugger with Memory inspection and Breakpoints
- Watchers for specific addresses
- Stack display when using *SP* pseudo-register
- Microprogramming with Visualization
- Sourcecode download and upload

## What is planned?

- Code inspection
- Extensive Manual
- MimaX architecture
- Custom Mnemonics (when using Microprogramming)
- Library Imports


